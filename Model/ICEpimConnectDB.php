<?php

namespace Iceshop\Icepimconnect\Model;

use \Magento\Framework\App\ObjectManager;
use Magento\Framework\Config\ConfigOptionsListConstants;

/**
 * Custom class for working with data in database
 */
class ICEpimConnectDB
{

    /*
     * Connection
     */
    public $connection = null;

    /*
     * Resource
     */
    public $resource = null;

    /*
     *  Instance of Magento Object Manager
     */
    public $objectManager = null;

    /*
     * Table for DB tables in Magento
     */
    public $tablePrefix = null;

    /*
     *  All conversions that exists at `icecat_imports_conversions_rules` table
     */
    public $conversionsRules = null;

    public function __construct()
    {
        // Init object manager instance
        if (!$this->connection) {
            $this->objectManager = ObjectManager::getInstance();
        }

        // Init instance of connection
        if (!$this->connection) {
            $resource = $this->objectManager->create('\Magento\Framework\App\ResourceConnection');
            $this->connection = $resource->getConnection(
                \Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION
            );
        }
        // Init instance of resource
        if (!$this->resource) {
            $this->resource = $this->objectManager->create('\Magento\Framework\App\ResourceConnection');
        }

        $deployConfig = $this->objectManager->get('Magento\Framework\App\DeploymentConfig');
        $this->tablePrefix = (string)$deployConfig->get(ConfigOptionsListConstants::CONFIG_PATH_DB_PREFIX);
    }


    /**
     * @param $sql
     * @param array $parameters
     * @return mixed
     */
    public function executeStatements($sql, $parameters = [])
    {
        $statement = str_replace('{prefix}', $this->tablePrefix, $sql);

        if (!empty($parameters))
            $query = $this->connection->query($statement, $parameters);
        else
            $query = $this->connection->query($statement);

        $fetch = $query->fetchAll();

        return $fetch;
    }

    public function fetchSingleValue($sql, $parameters = [], $field = false)
    {
        $return = false;

        $statement = str_replace('{prefix}', $this->tablePrefix, $sql);

        if (!empty($parameters))
            $query = $this->connection->query($statement, $parameters);
        else
            $query = $this->connection->query($statement);


        $fetchStatement = $query->fetch();

        if ($field) {
            if (isset($fetchStatement[$field])) {
                $return = $fetchStatement[$field];
            }
        }

        return $return;
    }

    /**
     * Save conversions
     *
     * @param $symbol
     * @param $original_id
     * @param $type
     * @param null $action
     */
    public function saveConversions($symbol, $original_id, $type, $action = null, $date = null)
    {
        if ($type == 'products') {
            // TODO: Add timezone selection to module settings
            date_default_timezone_set('Europe/Amsterdam');
            $this->connection->query(
                "INSERT INTO {$this->tablePrefix}icecat_imports_conversions_rules_{$type} (
                    imports_conversions_rules_symbol,
                    imports_conversions_rules_original,
                    {$action}
                )
                VALUES
                (
                    :imports_conversions_rules_symbol,
                    :imports_conversions_rules_original,
                    :{$action}
                )
                ON DUPLICATE KEY UPDATE {$action} = :{$action}
                ",
                [
                    ':imports_conversions_rules_original' => $original_id,
                    ':imports_conversions_rules_symbol' => $symbol,
                    ':' . $action => $date ?? date('Y-m-d H:i:s')
                ]
            );
        } else {
            $this->connection->query(
                "INSERT INTO {$this->tablePrefix}icecat_imports_conversions_rules_{$type} (
                imports_conversions_rules_symbol,
                imports_conversions_rules_original)
                VALUES
                (:imports_conversions_rules_symbol,
                :imports_conversions_rules_original)
                 ON DUPLICATE KEY UPDATE updated=NOW()
                ",
                [
                    ':imports_conversions_rules_original' => $original_id,
                    ':imports_conversions_rules_symbol' => $symbol
                ]
            );
        }
    }

    public function deleteProductsBulk($ids, $type)
    {
        if (!empty($ids)) {
            $implode = implode(',', $ids);
            $this->connection->query(
                "DELETE FROM {$this->tablePrefix}icecat_imports_conversions_rules_{$type}
                WHERE imports_conversions_rules_symbol IN ({$implode})"
            );
        }
    }

    public function deleteConversionsBulk($ids, $type)
    {
        $imports_conversions_rules_original = implode(',', $ids);
        $this->connection->query(
            "DELETE FROM {$this->tablePrefix}icecat_imports_conversions_rules_{$type}
            WHERE imports_conversions_rules_original IN (:imports_conversions_rules_original)",
            [
                ':imports_conversions_rules_original' => $imports_conversions_rules_original
            ]
        );
    }

    public function deleteConversion($id, $type)
    {
        $this->connection->query(
            "DELETE FROM {$this->tablePrefix}icecat_imports_conversions_rules_{$type}
            WHERE imports_conversions_rules_original = :imports_conversions_rules_original",
            [
                ':imports_conversions_rules_original' => $id
            ]
        );
    }

    public function _saveConversions($symbol, $original_id, $type, $action = null)
    {
        $this->saveConversions($symbol, $original_id, $type, $action);
    }

    /**
     * @param $type
     * @return mixed
     */
    public function _getConversionsRules($type)
    {
        $results = $this->connection->fetchAll(
            "SELECT `imports_conversions_rules_original`, `imports_conversions_rules_symbol`
              FROM {$this->tablePrefix}icecat_imports_conversions_rules_{$type} ORDER BY imports_conversions_rules_id;"
        );
        foreach ($results as $row) {
            $this->conversionsRules[$type][$row['imports_conversions_rules_original']] =
                $row['imports_conversions_rules_symbol'];
        }
        return $this->conversionsRules;
    }

    public function getProductConversions()
    {
        $return = [];

        $results = $this->connection->fetchAll("SELECT * FROM {$this->tablePrefix}icecat_imports_conversions_rules_products");

        foreach ($results as $row) {
            $return[$row['imports_conversions_rules_symbol']] = [
                'magentoId' => $row['imports_conversions_rules_original'],
                'onboarding' => $row['imports_conversions_rules_onboarding'],
                'content_updated' => $row['imports_conversions_rules_content_updated'],
                'logistics_updated' => $row['imports_conversions_rules_logistics_updated']
            ];
        }

        return $return;
    }

    /**
     * @param $id
     * @param $idType
     * @return void
     */
    public function getSingleProductConversion($magentoId)
    {
        $query = $this->connection->fetchAll("SELECT imports_conversions_rules_symbol as icepimId, imports_conversions_rules_content_updated as contentUpdated FROM {$this->tablePrefix}icecat_imports_conversions_rules_products WHERE imports_conversions_rules_original = :magentoId", [':magentoId' => $magentoId]);

        $result = [
            'icepimId' => $query['icepimId'] ?? '',
            'updated' => $query['contentUpdated'] ?? '0000-00-00 00:00:00',
        ];

        return $result;
    }

    /**
     * @param $original_id
     * @param $type
     * @return bool
     */
    public function getConversionRule($original_id, $type)
    {

        if (empty($this->conversionsRules[$type])) {
            $this->_getConversionsRules($type);
        }
        if (!empty($this->conversionsRules[$type][$original_id])) {
            return $this->conversionsRules[$type][$original_id];
        }

        return false;
    }

    public function setDbVariable($variableName, $variableValue) {
        $this->connection->query(
            "INSERT INTO {$this->tablePrefix}iceshop_icepimconnect_variables
                (
                  variable_name,
                  variable_value
                )
            VALUES
                (
                  :variable_name,
                  :variable_value
                )
            ON DUPLICATE KEY UPDATE variable_value = :variable_value",
            [
                ':variable_name' => $variableName,
                ':variable_value' => $variableValue
            ]
        );
    }

    public function getDbVariable($variableName) {
        $sql = "SELECT variable_value FROM {$this->tablePrefix}iceshop_icepimconnect_variables WHERE variable_name = :variable_name";

        $queryResult = $this->connection->fetchAll($sql, [':variable_name' => $variableName]);

        if(is_array($queryResult) && count($queryResult)) {
            return $queryResult[0]['variable_value'];
        } else {
            return null;
        }
    }

    public function slugify( $string, $separator = '-' ) {
        $accents_regex = '~&([a-z]{1,2})(?:acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i';
        $special_cases = array( '&' => 'and', "'" => '');
        $string = mb_strtolower( trim( $string ), 'UTF-8' );
        $string = str_replace( array_keys($special_cases), array_values( $special_cases), $string );
        $string = preg_replace( $accents_regex, '$1', htmlentities( $string, ENT_QUOTES, 'UTF-8' ) );
        $string = preg_replace("/[^a-z0-9]/u", "$separator", $string);
        $string = preg_replace("/[$separator]+/u", "$separator", $string);
        return $string;
    }
}
