<?php

namespace Iceshop\Icepimconnect\Model;

use Iceshop\Icepimconnect\Api\ICEpimConnectInterface;
use Magento\Catalog\Api\CategoryLinkManagementInterface;
use Magento\Catalog\Api\Data\ProductAttributeInterface;
use Magento\Catalog\Model\CategoryFactory;
use Magento\Catalog\Api\Data\CategoryInterfaceFactory;
use Magento\Catalog\Api\Data\CategoryInterface;
use Magento\Catalog\Model\CategoryRepository;
use Magento\Catalog\Model\Category;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ProductFactory;
use Magento\Catalog\Model\ProductRepository;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Indexer\Model\Indexer\CollectionFactory;
use Magento\Indexer\Model\IndexerFactory;
use Magento\Catalog\Api\SpecialPriceInterface;
use Magento\Catalog\Api\Data\SpecialPriceInterfaceFactory;
use Magento\CatalogInventory\Model\Stock\Status;
use Magento\CatalogUrlRewrite\Model\Category\CurrentUrlRewritesRegenerator;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Api\Data\ProductInterfaceFactory;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Eav\Model\Entity\Attribute;
use Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory as attributeCollectionFactory;
use Magento\Store\Model\StoreManagerInterface as storeManager;
use Magento\Catalog\Model\Product\Action as ProductAction;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Indexer\Model\Processor as processor;
use Magento\Eav\Setup\EavSetup;
use Magento\Catalog\Model\Config;
use Magento\Eav\Model\Config as eavConfig;
use Magento\Catalog\Api\Data\ProductLinkInterface;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;
use Magento\Catalog\Model\Product\Visibility;
use Magento\Catalog\Model\Product\Attribute\Source\Status as CategoryStatus;
use Magento\Framework\App\PageCache\Version;
use Magento\Framework\App\Cache\TypeListInterface;
use Magento\Framework\App\Cache\Frontend\Pool;
use Magento\Eav\Model\ResourceModel\Entity\Attribute\Group\CollectionFactory as GetGroupCollectionFactory;
use Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory as GetAttributeCollectionFactory;
use Magento\Indexer\Model\Indexer\StateFactory;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem;

/**
 * Defines the implementaiton class of the calculator service contract.
 */
class ICEpimConnect implements ICEpimConnectInterface
{
    /*
     * Constant for set mode `Scheduled` at indexer processes
     */
    const INDEXER_MODE_SCHEDULED = 'scheduled';

    /*
     * Constant for set mode `Update On Save` at indexer processes
     */
    const INDEXER_MODE_UPDATE_ON_SAVE = 'update_on_save';

    /*
     * Constant for set mode `Working` at indexer processes
     */
    const INDEXER_MODE_WORKING = 'working';

    public $arFiltersMap = [
        'product_id' => 'entity_id',
        'set' => 'attribute_set_id',
        'type' => 'type_id'
    ];

    /**
     * @var null
     */
    public $conversions_rules = null;

    /**
     * @var null
     */
    public $conversions_types = null;

    /*
     * Writer to `core_config_data` table
     */
    public $configWriter = null;

    /**
     * @var \Iceshop\Icepimconnect\Model\ICEpimConnectDB|null
     */
    public $connectDB = null;

    /**
     * @var bool
     */
    public $storeId = false;

    /**
     * @var array
     */
    public $stores = [];

    /**
     * @var bool
     */
    public $catalog_product_entity_table = false;

    /**
     * @var bool
     */
    public $ftp_connection = false;

    /**
     * @var IndexerFactory
     */
    protected $_indexerFactory;
    /**
     * @var CollectionFactory
     */
    protected $_indexerCollectionFactory;

    /**
     * @var ProductRepository
     */
    protected $productRepository;

    /**
     * @var ProductFactory
     */
    protected $productFactory;

    /**
     * @var Product
     */
    protected Product $product;

    /**
     * @var CategoryFactory
     */
    protected $categoryFactory;

    /**
     * @var Category
     */
    protected $category;

    /**
     * @var CategoryLinkManagementInterface
     */
    protected $categoryLinkManagement;

    /**
     * @var array
     */
    private $magentoCatIds = [];

    /**
     * @var array
     */
    private $magentoDeletedCatIds = [];

    /**
     * @var SpecialPriceInterface
     */
    protected $specialPrice;

    /**
     * @var SpecialPriceInterfaceFactory
     */
    protected $specialPriceFactory;

    /**
     * @var Status
     */
    protected $stockStatus;

    /**
     * @var CurrentUrlRewritesRegenerator
     */
    protected $currentUrlRewritesRegenerator;

    /**
     * @var ProductRepositoryInterface
     */
    protected $repository;

    /**
     * @var ProductInterfaceFactory
     */
    protected $productInterfaceFactory;

    /**
     * @var ProductInterface
     */
    protected $productInterface;

    /**
     * @var CategoryInterfaceFactory
     */
    protected $categoryInterfaceFactory;

    /**
     * @var CategoryRepository
     */
    protected $categoryRepositoryInterface;

    /**
     * @var Attribute
     */
    protected $attribute;

    /**
     * @var attributeCollectionFactory
     */
    protected $attributeCollectionFactory;

    /**
     * @var storeManager
     */
    protected $storeManager;

    /**
     * @var ProductAction
     */
    protected $productAction;

    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfigInterface;

    /**
     * @var processor
     */
    protected $processor;

    /**
     * @var EavSetup
     */
    protected $eavSetup;

    protected $objectManager;

    /**
     * @var Config
     */
    protected $config;

    /**
     * @var eavConfig
     */
    protected $eavConfig;

    /**
     * @var CategoryInterface
     */
    protected $categoryInterface;

    /**
     * @var ProductLinkInterface
     */
    protected $productLinkInterface;

    /**
     * @var ProductCollectionFactory
     */
    protected $productCollectionFactory;

    /**
     * @var TypeListInterface
     */
    protected $cacheTypeList;

    /**
     * @var Pool
     */
    protected $cacheFrontendPool;

    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;

    /**
     * @var \Magento\Framework\Api\FilterBuilder
     */
    protected $filterBuilder;

    /**
     * @var \Magento\Framework\Api\Search\FilterGroup
     */
    protected $filterGroup;

    protected $getGroupCollectionFactory;

    protected $getAttributeCollectionFactory;

    protected $stateFactory;

    protected $filesystem;

    protected $directoryList;

    protected $searchCriteria;

    protected $img_attrs;

    protected $columnName = 'entity_id';


    public function __construct(
        IndexerFactory                  $indexerFactory,
        CollectionFactory               $indexerCollectionFactory,
        ProductRepository               $productRepository,
        Product                         $product,
        ProductFactory                  $productFactory,
        CategoryFactory                 $categoryFactory,
        Category                        $category,
        CategoryLinkManagementInterface $categoryLinkManagement,
        SpecialPriceInterface           $specialPrice,
        SpecialPriceInterfaceFactory    $specialPriceFactory,
        Status                          $stockStatus,
        CurrentUrlRewritesRegenerator   $currentUrlRewritesRegenerator,
        ProductInterfaceFactory         $productInterfaceFactory,
        ProductRepositoryInterface      $repository,
        ProductInterface                $productInterface,
        CategoryInterfaceFactory        $categoryInterfaceFactory,
        CategoryRepository              $categoryRepositoryInterface,
        Attribute                       $attribute,
        attributeCollectionFactory      $attributeCollectionFactory,
        storeManager                    $storeManager,
        ProductAction                   $productAction,
        ScopeConfigInterface            $scopeConfigInterface,
        processor                       $processor,
        EavSetup                        $eavSetup,
        Config                          $config,
        eavConfig                       $eavConfig,
        CategoryInterface               $categoryInterface,
        ProductLinkInterface            $productLinkInterface,
        ProductCollectionFactory        $productCollectionFactory,
        TypeListInterface               $cacheTypeList,
        Pool                            $cacheFrontendPool,
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria,
        \Magento\Framework\Api\FilterBuilder $filterBuilder,
        \Magento\Framework\Api\Search\FilterGroup $filterGroup,
        GetGroupCollectionFactory       $getGroupCollectionFactory,
        GetAttributeCollectionFactory   $getAttributeCollectionFactory,
        StateFactory                    $stateFactory,
        DirectoryList                   $directoryList,
        Filesystem                      $filesystem

    )
    {
        $this->connectDB = new ICEpimConnectDB();
        $this->_indexerFactory = $indexerFactory;
        $this->_indexerCollectionFactory = $indexerCollectionFactory;
        $this->catalog_product_entity_table = $this->connectDB->resource->getTableName('catalog_product_entity');
        $this->productRepository = $productRepository;
        $this->productFactory = $productFactory;
        $this->product = $product;
        $this->categoryFactory = $categoryFactory;
        $this->category = $category;
        $this->categoryLinkManagement = $categoryLinkManagement;
        $this->specialPrice = $specialPrice;
        $this->specialPriceFactory = $specialPriceFactory;
        $this->stockStatus = $stockStatus;
        $this->currentUrlRewritesRegenerator = $currentUrlRewritesRegenerator;
        $this->repository = $repository;
        $this->productInterfaceFactory = $productInterfaceFactory;
        $this->productInterface = $productInterface;
        $this->categoryInterfaceFactory = $categoryInterfaceFactory;
        $this->categoryRepositoryInterface = $categoryRepositoryInterface;
        $this->attribute = $attribute;
        $this->attributeCollectionFactory = $attributeCollectionFactory;
        $this->storeManager = $storeManager;
        $this->productAction = $productAction;
        $this->scopeConfigInterface = $scopeConfigInterface;
        $this->processor = $processor;
        $this->eavSetup = $eavSetup;
        $this->objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->config = $config;
        $this->eavConfig = $eavConfig;
        $this->categoryInterface = $categoryInterface;
        $this->productLinkInterface = $productLinkInterface;
        $this->productCollectionFactory = $productCollectionFactory;
        $this->cacheTypeList = $cacheTypeList;
        $this->cacheFrontendPool = $cacheFrontendPool;
        $this->searchCriteria = $searchCriteria;
        $this->filterBuilder = $filterBuilder;
        $this->filterGroup = $filterGroup;
        $this->getGroupCollectionFactory = $getGroupCollectionFactory;
        $this->getAttributeCollectionFactory = $getAttributeCollectionFactory;
        $this->stateFactory = $stateFactory;
        $this->directoryList = $directoryList;
        $this->filesystem = $filesystem;

        $magentoEdition = $this->objectManager->get('Magento\Framework\App\ProductMetadataInterface')->getEdition();

        if ($magentoEdition === 'Enterprise') {
            $this->columnName = 'row_id';
        }
    }


    /**
     * @return mixed
     */
    public function flushCache()
    {
        $response = [];

        $cacheTypes = [
            'config',
            'layout',
            'block_html',
            'collections',
            'reflection',
            'db_ddl',
            'eav',
            'config_integration',
            'config_integration_api',
            'full_page',
            'translate',
            'config_webservice'
        ];

        try {
            foreach ($cacheTypes as $cacheType) {
                $this->cacheTypeList->cleanType($cacheType);
            }

            foreach ($this->cacheFrontendPool as $cacheFrontend) {
                $cacheFrontend->getBackend()->clean();
            }

            $response = ['Status' => 'Success'];
        } catch (\Throwable $e) {
            $response = ['Status' => 'Error: ' . $e->getMessage()];
        }

        return json_encode($response);
    }

    /**
     * Get version of Magento shop
     * @return string
     */

    public function getICEshopIcepimconnectorExtensionVersion()
    {
        $this->getConfigWriter();
        $this->connectDB->setDbVariable('icepimconnect_content_last_start', date('Y-m-d H:i:s'));
        $productMetadata = $this->connectDB->objectManager->get('Magento\Framework\App\ProductMetadataInterface');
        $version = $productMetadata->getVersion();
        return json_encode((string)$version);
    }

    /**
     * Create _configWriter for save to `core_config_data`
     * @return null
     */
    public function getConfigWriter()
    {
        if (!$this->configWriter) {
            $this->configWriter = $this->connectDB->objectManager->create(
                'Magento\Framework\App\Config\Storage\WriterInterface'
            );
        }
        return $this->configWriter;
    }

    /**
     * Get products from shop
     * @param mixed $data
     * @return string
     */
    public function getProductsBatch($data)
    {
        $data = json_decode($data, true);
        $storeId = 0;
        $result = [];
        $page = null;
        $pageSize = null;

        if (!empty($data) && is_array($data)) {
            $page = (!empty($data['page'])) ? $data['page'] : null;
            $pageSize = (!empty($data['page_size'])) ? $data['page_size'] : null;
        }

        $mpn = $this->scopeConfigInterface->getValue(
            'iceshop_Icepimconnect_settings/icepimconnect_products_mapping/products_mapping_mpn'
        );
        $brandName = $this->scopeConfigInterface->getValue(
            'iceshop_Icepimconnect_settings/icepimconnect_products_mapping/products_mapping_brand'
        );
        $ean = $this->scopeConfigInterface->getValue(
            'iceshop_Icepimconnect_settings/icepimconnect_products_mapping/products_mapping_gtin'
        );
        $activeIce = $this->scopeConfigInterface->getValue(
            'iceshop_Icepimconnect_settings/icepimconnect_products_mapping/products_active_ice'
        );

        if (empty($mpn) || empty($brandName) || empty($ean) || empty($activeIce)) {
            $result['error'] = 'MappingError';
        }

        $activeIceOptions = $this->connectDB->objectManager->create(
            'Magento\Eav\Model\Config'
        )
            ->getAttribute(
                ProductAttributeInterface::ENTITY_TYPE_CODE,
                $activeIce
            );

        $options = $activeIceOptions->getSource()->getAllOptions();
        if (!empty($options)) {
            foreach ($options as $value) {
                if (!empty($value['label'])) {
                    if ($value['label'] == 'Yes') {
                        $activeIceId = $value['value'];
                        break;
                    }
                }
            }
        }

        $collection = $this->connectDB->objectManager->create('Magento\Catalog\Model\ResourceModel\Product\Collection')
            ->addStoreFilter($storeId)
            ->addAttributeToSelect('name')
            ->addAttributeToSelect($mpn)
            ->addAttributeToSelect($brandName)
            ->addAttributeToSelect($ean)
            ->addAttributeToFilter($activeIce, $activeIceId);

        if ($page !== null && $pageSize !== null) {
            $collection = $collection
                ->setPageSize($pageSize)
                ->setCurPage($page);
        }

        $totalProducts = $collection->getSize();
        if ($totalProducts >= ($pageSize * ($page - 1))) {
            foreach ($collection as $product) {
                // get products updated dates
                $getConversion = $this->connectDB->getSingleProductConversion($product->getID());

                $item = [
                    'magentoId' => $product->getID(),
                    'mpn' => $product->getData($mpn),
                    'ean' => $product->getData($ean),
                ];

                $brandNameAttributeType = $product->getResource()
                    ->getAttribute($brandName)
                    ->getFrontend()
                    ->getInputType();

                switch ($brandNameAttributeType) {
                    case 'select':
                    case 'dropdown':
                        $item['brand'] = $product->getAttributeText($brandName);
                        break;
                    case 'text':
                        $item['brand'] = $product->getData($brandName);
                        break;
                }

                $item['icepimId'] = $getConversion['icepimId'];
                $item['updated'] = $getConversion['updated'];

                $result['products'][] = $item;

                unset($item);
            }
        }

        return json_encode($result);
    }

    /**
     * Get products from shop
     * @param mixed $data
     * @return string
     */
    public function getProductsBatch1($data)
    {
        $data = json_decode($data, true);
        $storeId = 0;
        $result = [];
        $page = null;
        $page_size = null;

        if (!empty($data) && is_array($data)) {
            $page = (!empty($data['page'])) ? $data['page'] : null;
            $page_size = (!empty($data['page_size'])) ? $data['page_size'] : null;
        }
        $collection = $this->connectDB->objectManager->create('Magento\Catalog\Model\ResourceModel\Product\Collection')
            ->addStoreFilter($storeId)
            ->addAttributeToSelect('name')
            ->addAttributeToSelect('updated_ice');

        $optionId = false;
        $eav = $this->connectDB->objectManager->get('\Magento\Eav\Model\Config');
        $attribute = $eav->getAttribute('catalog_product', 'active_ice')->getData();
        if (isset($attribute['attribute_id'])) {
            $options = $eav->getAttribute('catalog_product', 'active_ice')->getSource()->getAllOptions();
            foreach ($options as $option) {
                if ($option['label'] == 'Yes') {
                    $optionId = $option['value'];
                    break;
                }
            }
        }

        if ($optionId !== false) {
            $sql = "UPDATE {$this->connectDB->resource->getTableName('catalog_product_entity')}
                    SET active_ice = " . $optionId . "  WHERE active_ice IS NULL; ";

            $this->connectDB->connection->query($sql);
            $collection->addFieldToFilter('active_ice', $optionId);
        }

        $scopeConfig = $this->connectDB->objectManager->get('\Magento\Framework\App\Config\ScopeConfigInterface');
        $mpn_attribute_code = $scopeConfig->getValue(
            'iceshop_icepimconnect/icepimconnect_products_mapping/products_mapping_mpn'
        );
        if (!empty($mpn_attribute_code)) {
            $collection->addAttributeToSelect($mpn_attribute_code);
        }
        $brand_name_attribute_code = $scopeConfig->getValue(
            'iceshop_icepimconnect/icepimconnect_products_mapping/products_mapping_brand'
        );
        if (!empty($brand_name_attribute_code)) {
            $collection->addAttributeToSelect($brand_name_attribute_code);
        }
        $ean_attribute_code = $scopeConfig->getValue(
            'iceshop_icepimconnect/icepimconnect_products_mapping/products_mapping_gtin'
        );
        if (!empty($ean_attribute_code)) {
            $collection->addAttributeToSelect($ean_attribute_code);
        }

        $brand_name_attribute_type = false;
        if ($page !== null && $page_size !== null) {
            $collection = $collection
                ->setPageSize($page_size)
                ->setCurPage($page);
        }
        $total_products = $collection->getSize();
        if ($total_products >= ($page_size * ($page - 1))) {
            foreach ($collection as $product) {
                $item = [
                    'product_id' => $product->getId(),
                    'updated' => $product->getData('updated_ice')
                ];

                if (!empty($mpn_attribute_code)) {
                    $item['mpn'] = $product->getData($mpn_attribute_code);
                }
                if (!empty($brand_name_attribute_code)) {
                    if ($brand_name_attribute_type == false) {
                        $brand_name_attribute_type = $product->getResource()
                            ->getAttribute($brand_name_attribute_code)
                            ->getFrontend()
                            ->getInputType();
                    }
                    switch ($brand_name_attribute_type) {
                        case 'select':
                        case 'dropdown':
                            $item['brand_name'] = $product->getAttributeText($brand_name_attribute_code);
                            break;
                        case 'text':
                            $item['brand_name'] = $product->getData($brand_name_attribute_code);
                            break;
                    }
                }
                if (!empty($ean_attribute_code)) {
                    $item['ean'] = $product->getData($ean_attribute_code);
                }
                $result['products'][] = $item;
            }
        }
        $defaultSetId = $this->connectDB->objectManager->create('Magento\Catalog\Model\Product')
            ->getDefaultAttributeSetid();
        $result['default_attribute_set'] = $defaultSetId;

        return json_encode($result);
    }

    public function getWebsiteCodeByStoreId($storeId)
    {
        $websiteCode = null;
        try {
            $websiteId = $this->storeManager->getStore($storeId)->getWebsiteId();
            $websiteCode = $this->storeManager->getWebsite($websiteId)->getCode();
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            // Log the exception
            $e->getMessage();
        }

        return $websiteCode;
    }

    /**
     * Get language mapping
     * @return string
     */
    public function getLanguageMapping()
    {
        $scopeConfig = $this->connectDB->objectManager->get('\Magento\Framework\App\Config\ScopeConfigInterface');
        $is_multilingual = $scopeConfig->getValue(
            'iceshop_Icepimconnect_settings/Icepimconnect_language_mapping/multilingual_mode'
        );
        $attribute_update_required = $scopeConfig->getValue(
            'iceshop_Icepimconnect_settings/icepimconnect_service_settings/products_update_attributes'
        );

        $attribute_sort_order_update_required = 0;
        $attribute_labels_update_required = 0;
        $import_main_attributes = [];
        if (!isset($attribute_update_required)) {
            $attribute_update_required = 1;
        }
        if ($attribute_update_required == 1) {
            //fetch label/sort_order settings
            $attribute_sort_order_update_required = $scopeConfig->getValue(
                'iceshop_Icepimconnect_settings/icepimconnect_service_settings/products_update_sort_order'
            );
            $attribute_labels_update_required = $scopeConfig->getValue(
                'iceshop_Icepimconnect_settings/icepimconnect_service_settings/update_attribute_labels'
            );
        }
        if ($is_multilingual == 1) {
            //multilingual mode
            $mapping = $scopeConfig->getValue(
                'iceshop_Icepimconnect_settings/Icepimconnect_language_mapping/multilingual_values'
            );

            //fix store_id = 0
            $mapping = json_decode($mapping, true);
            if (isset($mapping[$this->_getStoreId()])) {
                $mapping[0] = $mapping[$this->_getStoreId()];
                $mapping[0]['store_id'] = 0;
                $mapping = json_encode($mapping);
            } else {
                $mapping = json_encode($mapping);
            }

            $mapping = json_decode($mapping, true);
            foreach ($mapping as $store_id => $map) {
                $mapping[$store_id]['website'] = $this->getWebsiteCodeByStoreId($store_id);
            }

            $mapping = json_encode($mapping);
        } else {
            //one language mode
            $mapping = [
                (object)[
                    'store_id' => 0,
                    'value' => $scopeConfig->getValue(
                        'iceshop_Icepimconnect_settings/Icepimconnect_language_mapping/main_language_single'
                    )
                ],
                (object)[
                    'store_id' => $this->_getStoreId(),
                    'value' => $scopeConfig->getValue(
                        'iceshop_Icepimconnect_settings/Icepimconnect_language_mapping/main_language_single'
                    )
                ],
                (object)[
                    'store_id' => 9999,
                    'value' => $scopeConfig->getValue(
                        'iceshop_Icepimconnect_settings/Icepimconnect_language_mapping/insurance_language_single'
                    )
                ]
            ];

            $mapping = json_encode($mapping);
        }

        $selectedWebsites = $scopeConfig->getValue(
            'iceshop_Icepimconnect_settings/icepimconnect_products_mapping/products_websites'
        );

        $setCategoryStatus = $scopeConfig->getValue(
            'iceshop_Icepimconnect_settings/icepimconnect_products_mapping/category_status'
        );

        $response = [
            'multi' => $is_multilingual,
            'mapping' => $mapping,
            'websites' => $selectedWebsites,
            'exact_language_import' => $scopeConfig->getValue(
                'iceshop_Icepimconnect_settings/icepimconnect_language_mapping/strict_language_import'
            ),
            'attribute_update_required' => $attribute_update_required,
            'attribute_sort_order_update_required' => $attribute_sort_order_update_required,
            'attribute_labels_update_required' => $attribute_labels_update_required,
            'import_main_attributes' => $import_main_attributes,
            'setCategoryStatus' => $setCategoryStatus
        ];

        // Import main mapping attributes if empty values
        $main_attributes_mapping = $this->getICEShopMapping();
        if (($main_attributes_mapping['mpn'] != '') && ($main_attributes_mapping['brand_name'] != '')
            && ($main_attributes_mapping['ean'] != '')
        ) {
            $import_main_attributes = [];
            $import_main_attributes['mapping'] = $main_attributes_mapping;
            $import_main_attributes['ean'] = $scopeConfig->getValue(
                'iceshop_Icepimconnect_settings/icepimconnect_products_mapping/import_gtin'
            );
            $import_main_attributes['brand_mpn'] = $scopeConfig->getValue(
                'iceshop_Icepimconnect_settings/icepimconnect_products_mapping/import_brand_mpn'
            );
            $response['import_main_attributes'] = $import_main_attributes;
        }
        if (!empty($main_attributes_mapping) && array_key_exists('description', $main_attributes_mapping)) {
            $response['description'] = $main_attributes_mapping['description'];
        }
        return json_encode($response);
    }

    public function getICEShopMapping()
    {
        $scopeConfig = $this->connectDB->objectManager->get('\Magento\Framework\App\Config\ScopeConfigInterface');
        $mapping = [];
        $mapping['mpn'] = $scopeConfig->getValue(
            'iceshop_Icepimconnect_settings/icepimconnect_products_mapping/products_mapping_mpn'
        );
        $mapping['brand_name'] = $scopeConfig->getValue(
            'iceshop_Icepimconnect_settings/icepimconnect_products_mapping/products_mapping_brand'
        );
        $mapping['ean'] = $scopeConfig->getValue(
            'iceshop_Icepimconnect_settings/icepimconnect_products_mapping/products_mapping_gtin'
        );
        if ($scopeConfig->getValue(
                'iceshop_Icepimconnect_settings/icepimconnect_products_mapping/default_description_attributes'
            ) == 0
        ) {
            $mapping['description']['name'] = $scopeConfig->getValue(
                'iceshop_Icepimconnect_settings/icepimconnect_products_mapping/name_attribute'
            );
            $mapping['description']['short_description'] = $scopeConfig->getValue(
                'iceshop_Icepimconnect_settings/icepimconnect_products_mapping/short_description_attribute'
            );
            $mapping['description']['long_description'] = $scopeConfig->getValue(
                'iceshop_Icepimconnect_settings/icepimconnect_products_mapping/long_description_attribute'
            );
            $mapping['description']['icecat_products_name'] = $scopeConfig->getValue(
                'iceshop_Icepimconnect_settings/icepimconnect_products_mapping/icecat_products_name_attribute'
            );
        }
        return $mapping;
    }

    /**
     * Get attribute sets from Magento shop
     * @param array $data
     * @return string
     */
    public function catalogProductAttributeSetList($data)
    {
        $data = json_decode($data, true);
        $entityTypeId = $this->connectDB->objectManager->get('\Magento\Catalog\Model\Product')->getResource()
            ->getEntityType()->getId();

        // limits for attribute sets
        $limited = false;
        if (!empty($data)) {
            if (array_key_exists('start', $data)) {
                $start = (int)$data['start'];
                if ($start < 0) {
                    $start = 0;
                }
                $limited = true;
            }
            if (array_key_exists('limit', $data)) {
                $limit = (int)$data['limit'];
                if ($limit <= 0) {
                    $limit = 50;
                }
                $limited = true;
            }
        }
        // Get all attribute sets from `eav_attribute_set` table
        $eavSet = "SELECT `attribute_set_id`, `attribute_set_name` FROM `{prefix}eav_attribute_set`
        WHERE `entity_type_id` = :entityTypeId ";

        if ($limited == true) {
            $eavSet .= " LIMIT {$start}, {$limit}";
        }
        $fetchAttributeSets = $this->connectDB->executeStatements($eavSet, [':entityTypeId' => $entityTypeId]);


        $result = [];
        if (!empty($fetchAttributeSets)) {
            foreach ($fetchAttributeSets as $attributeSet) {
                // Get all attribute groups for needed attribute set, because we also then map and import they
                $groupSet = "SELECT `attribute_group_id`, `attribute_group_name` FROM `{prefix}eav_attribute_group`
                WHERE `attribute_set_id` = :attributeSetId ";

                $fetchGroups = $this->connectDB->executeStatements($groupSet,
                    [
                        ':attributeSetId' => $attributeSet['attribute_set_id']
                    ]
                );

                $groups = [];
                foreach ($fetchGroups as $group) {
                    $groups[] = [
                        'id' => $group['attribute_group_id'],
                        'name' => $group['attribute_group_name'],
                        'external_id' => $this->connectDB->getConversionRule(
                            $group['attribute_group_id'],
                            'attribute_group'
                        )
                    ];
                }

                // Get all attributes that belong needed attribute set and has
                // needed entity_type (by default `catalog_product`)
                $eavAttributes = "SELECT eav_a.`attribute_id`, eav_a.`attribute_code` FROM
                `{prefix}eav_entity_attribute`eav_ea  LEFT JOIN `{prefix}eav_attribute` eav_a
                ON eav_ea.`attribute_id` = eav_a.`attribute_id`
                WHERE eav_ea.`entity_type_id` = :entityTypeId
                AND eav_ea.attribute_set_id = :attributeSetId ; ";

                $fetchAttributes = $this->connectDB->executeStatements($eavAttributes, [
                    ':entityTypeId' => $entityTypeId,
                    ':attributeSetId' => $attributeSet['attribute_set_id']
                ]);

                $attributes = [];
                foreach ($fetchAttributes as $attribute) {
                    if ($conversion = $this->connectDB->getConversionRule($attribute['attribute_id'], 'attribute')) {
                        $attributes_arr[] = [
                            'id' => $attribute['attribute_id'],
                            'name' => $attribute['attribute_code'],
                            'external_id' => $conversion
                        ];
                    }
                }

                $result[] = [
                    'set_id' => $attributeSet['attribute_set_id'],
                    'name' => $attributeSet['attribute_set_name'],
                    'external_id' => $this->connectDB->getConversionRule(
                        $attributeSet['attribute_set_id'],
                        'attribute_set'
                    ),
                    'groups' => $groups,
                    'attributes' => $attributes
                ];
            }
        }
        return json_encode($result);
    }

    /**
     * Get store id
     * @return mixed
     */
    public function _getStoreId()
    {
        if (!$this->storeId) {
            $storeManager = $this->connectDB->objectManager->get('\Magento\Store\Model\StoreManagerInterface');
            $this->storeId = $storeManager->getStore()->getId();
        }

        return $this->storeId;
    }

    /**
     * @param $data
     * @return string
     */
    public function getProductAttributeList($data)
    {
        $data = json_decode($data, true);
        $response = [];
        if (!empty($data) && is_array($data) && !empty($data['items'])) {
            $attribute_api_model = $this->connectDB->objectManager->create(
                'Iceshop\Icepimconnect\Model\ICEpimConnectCatalogProductAttributeApi'
            );

            $attribute_api_model->connectDB = $this->connectDB;
            $attribute_api_model->product_entity_type_id = $this->connectDB->objectManager->create(
                '\Magento\Catalog\Model\Product'
            )->getResource()->getEntityType()->getId();
            $attribute_api_model->eav_attribute = $this->connectDB->objectManager->create(
                'Magento\Catalog\Model\ResourceModel\Eav\Attribute'
            );
            $attribute_api_model->eav_model_config = $this->connectDB->objectManager->get(
                '\Magento\Eav\Model\Config'
            );
            $attribute_api_model->eav_entity_attribute_table = $this->connectDB->resource->getTableName(
                'eav_entity_attribute'
            );
            $attribute_api_model->eav_attribute_table = $this->connectDB->resource->getTableName('eav_attribute');
            $attribute_api_model->catalog_eav_attribute_table = $this->connectDB->resource->getTableName(
                'catalog_eav_attribute'
            );

            foreach ($data['items'] as $attribute_set_id) {
                try {
                    $attribute_list = $attribute_api_model->getAttributes($attribute_set_id);
                    $response[$attribute_set_id] = $attribute_list;
                } catch (\Exception $e) {
                    $response['API_ERROR'][$attribute_set_id] = [
                        'comment' => $e->getMessage(),
                        'status' => 'Error while fetching attribute list'
                    ];
                }
            }
        }
        return json_encode($response);
    }

    private function findUnusedAttributes($attributeSetId)
    {
        $attributeIds = [];

        $groupCollection = $this->getGroupCollectionFactory->create()
            ->setAttributeSetFilter($attributeSetId)
            ->load();

        foreach ($groupCollection as $group) {
            $groupAttributesCollection = $this->getAttributeCollectionFactory->create()
                ->setAttributeGroupFilter($group->getId())
                ->addVisibleFilter()
                ->addFilter('is_user_defined', 1)
                ->load();

            foreach ($groupAttributesCollection->getItems() as $attribute) {
                if (substr($attribute->getAttributeCode(), 0, 2) === 'i_') {
                    array_push($attributeIds, $attribute->getAttributeCode());
                }
            }
        }

        return $attributeIds;
    }

    private function deleteUnusedAttributeGroups($attributeSetId)
    {
        $groupIds['Success'] = [];
        $groupIds['Error'] = [];

        $groupCollection = $this->getGroupCollectionFactory->create()
            ->setAttributeSetFilter($attributeSetId)
            ->load();

        $groupResource = $this->connectDB->objectManager->create(
            'Magento\Eav\Api\AttributeGroupRepositoryInterface'
        );

        foreach ($groupCollection as $group) {
            $groupAttributesCollection = $this->getAttributeCollectionFactory->create()
                ->setAttributeGroupFilter($group->getId())
                ->load();

            $findInTable = $this->connectDB->getConversionRule($group->getId(), 'attribute_group');

            if (empty($groupAttributesCollection->getItems()) && $findInTable !== false) {
                try {
                    $groupResource->deleteById($group->getId());
                    array_push($groupIds['Success'], $group->getId());
                } catch (\Throwable $e) {
                    array_push($groupIds['Error'], $group->getId());
                    continue;
                }
            }
        }

        return $groupIds;
    }

    private function findErroredAttributeSet($attributeSetId)
    {
        $groupIds = [];
        try {
            $groupCollection = $this->getGroupCollectionFactory->create()
                ->setAttributeSetFilter($attributeSetId)
                ->load();

            foreach ($groupCollection as $group) {
                array_push($groupIds, $group->getAttributeGroupCode());
            }

            if (!$groupIds) {
                return false;
            }
        } catch (\Exception $exception) {
            return true;
        }

        return true;
    }

    /**
     * Save sets, attributes, groups for products
     * with data that get from server
     *
     * @param mixed $data
     * @return bool|string
     */
    public function saveAttributeSetBatch($data)
    {
        ini_set('memory_limit', -1);
        ini_set('max_execution_time', 0);
        ini_set('post_max_size', '256M');

        $response = [];

        $data = json_decode($data, true);

        $entityProductTypeId = $this->connectDB->objectManager->create('Magento\Eav\Model\Entity\Type')
            ->loadByCode(
                'catalog_product'
            )->getId();

        // initialize additional class and set needed instances
        $attribute_api_model = $this->connectDB->objectManager->create(
            'Iceshop\Icepimconnect\Model\ICEpimConnectCatalogProductAttributeApi'
        );
        $attribute_api_model->connectDB = $this->connectDB;


        $group_id = false;
        $attributesLast = [];

        if (!empty($data) && is_array($data)) {
            if (!empty($data['set'])) {
                if (empty($data['set']['magento_id'])) {
                    //create new attribute set
                    try {
                        $checkAttributeSetId = $this->connectDB->objectManager->get(
                            'Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\Collection'
                        )->setEntityTypeFilter($entityProductTypeId)
                            ->addFieldToFilter('attribute_set_name', $data['set'][0])
                            ->getFirstItem()
                            ->getAttributeSetId();


                        if ($checkAttributeSetId && !empty($data['set'][1]['external_id'])) {
                            $this->connectDB->saveConversions($data['set'][1]['external_id'], $checkAttributeSetId, 'attribute_set');
                            $response['set'] = [
                                'external_id' => $data['set'][1]['external_id'],
                                'comment' => 'Attribute Set created successfully',
                                'magento_id' => $checkAttributeSetId
                            ];
                            $set_id = $checkAttributeSetId;
                        } else {

                            $attributeSet = $this->connectDB->objectManager->create(
                                'Magento\Eav\Model\Entity\Attribute\Set'
                            );
                            $entityTypeId = $entityProductTypeId;
                            $attributeSet->setData([
                                'attribute_set_name' => $data['set'][0],
                                'entity_type_id' => $entityTypeId,
                                'sort_order' => 200,
                            ]);

                            if ($attributeSet->validate()) {
                                $attributeSet->save();
                                $attributeSet->initFromSkeleton($this->product->getDefaultAttributeSetId());
                                $attributeSet->save();
                                $set_id = $attributeSet->getId();
                                if (!empty($data['set'][1]['external_id'])) {
                                    $this->connectDB->saveConversions(
                                        $data['set'][1]['external_id'],
                                        $set_id,
                                        'attribute_set'
                                    );
                                }
                                $response['set'] = [
                                    'external_id' => $data['set'][1]['external_id'],
                                    'comment' => 'Attribute Set created successfully',
                                    'magento_id' => $set_id
                                ];
                            } else {
                                $response['API_ERROR']['set'][] = [
                                    'external_id' => $data['set'][1]['external_id'],
                                    'comment' => 'Validation error in attribute set',
                                    'magento_id' => null
                                ];
                            }
                        }
                    } catch (\Exception $e) {
                        $response['API_ERROR']['set'][] = [
                            'external_id' => $data['set'][1]['external_id'],
                            'comment' => 'error with set: ' . $e->getMessage(),
                            'magento_id' => null
                        ];
                    } catch (\Magento\Framework\Exception\LocalizedException $e) {
                        $response['API_ERROR']['set'][] = [
                            'external_id' => $data['set'][1]['external_id'],
                            'comment' => 'Error with validation attribute set (Message: ' . $e->getMessage() . ')',
                            'magento_id' => null
                        ];
                    }
                } else {
                    //fetch only attribute set ID
                    $set_id = $data['set']['magento_id'];
                    $response['set'] = [
                        'external_id' => $data['set'][1]['external_id'],
                        'comment' => 'Attribute set exists at store',
                        'magento_id' => $set_id
                    ];
                }

                if (isset($set_id) && !empty($data['set']['groups'])) {

                    $groupCollection = $this->connectDB->objectManager->get(
                        '\Magento\Eav\Model\ResourceModel\Entity\Attribute\Group\Collection'
                    )
                        ->setAttributeSetFilter($set_id)
                        ->load();

                    foreach ($data['set']['groups'] as $group) {
                        if (empty($group['magento_id'])) {
                            //create new attribute group
                            if (!empty($group['groupName']) && !empty($group['data'])) {
                                try {
                                    $group_id = $this->groupAdd(
                                        $set_id,
                                        $group['groupName'] ?? $group['data']['external_id'],
                                        $group['data'],
                                        $groupCollection
                                    );

                                    if (!is_int($group_id)) {
                                        $response['API_ERROR']['set'][] = [
                                            'external_id' => $group['data']['external_id'],
                                            'comment' => $group_id,
                                            'magento_id' => null
                                        ];
                                    } else {
                                        $response['set']['groups'][$group_id] = [
                                            'external_id' => $group['data']['external_id'],
                                            'comment' => 'Attribute group `' . $group['groupName'] .
                                                '` created successfully',
                                            'magento_id' => $group_id
                                        ];
                                    }
                                } catch (\Magento\Framework\Exception\LocalizedException $e) {
                                    $response['API_ERROR']['set'][] = [
                                        'external_id' => $group['data']['external_id'],
                                        'comment' => 'error with group: ' . $e->getMessage(),
                                        'magento_id' => null
                                    ];
                                    continue;
                                } catch (\Exception $e) {
                                    $response['API_ERROR']['set'][] = [
                                        'external_id' => $group['data']['external_id'],
                                        'comment' => 'error with group: ' . $e->getMessage(),
                                        'magento_id' => null
                                    ];
                                    continue;
                                }
                            }
                        } else {
                            //group already exists
                            $group_id = $group['magento_id'];

                            $this->eavSetup->updateAttributeGroup(
                                $this->connectDB->objectManager->create('Magento\Eav\Model\Entity\Type')->loadByCode('catalog_product')->getId(),
                                $set_id,
                                $group_id,
                                'attribute_group_name',
                                $group['groupName']
                            );

                            $response['set']['groups'][$group_id] = [
                                'external_id' => $group['external_id'],
                                'comment' => 'Attribute group already exists',
                                'magento_id' => $group_id
                            ];
                        }

                        if ($group_id && !empty($group['attributes']) && is_array($group['attributes'])) {
                            $response['set']['groups'][$group_id]['attributes'] = [];
                            //attributes loop (create and link to the attribute set)
                            foreach ($group['attributes'] as $attribute) {
                                $attribute_id = null;
                                // create new attribute

                                try {
                                    $attribute_id = $attribute_api_model->create($attribute);
                                    $response['set']['groups'][$group_id]['attributes']
                                    [$attribute['external_id']] = [
                                        'external_id' => $attribute['external_id'],
                                        'comment' => 'Attribute created successfully',
                                        'magento_id' => $attribute_id,
                                        'attribute_code' => $attribute['attribute_code'],
                                        'type' => $attribute['frontend_input']
                                    ];

                                    $attributesLast[] = $attribute['external_id'];
                                } catch (\Magento\Framework\Exception\LocalizedException $e) {
                                    $response['API_ERROR']['set'][] = [
                                        'external_id' => $attribute['external_id'],
                                        'comment' => 'Error with attribute: ' . $e->getMessage(),
                                        'magento_id' => null
                                    ];
                                } catch (\Exception $e) {
                                    $response['API_ERROR']['set'][] = [
                                        'external_id' => $attribute['external_id'],
                                        'comment' => 'Error with attribute: ' . $e->getMessage(),
                                        'magento_id' => null
                                    ];
                                }

                                //link attribute to attribute set
                                if (!empty($attribute_id)) {
                                    try {
                                        if (array_key_exists('sort_order', $attribute)) {
                                            $attribute_api_model->attributeAdd(
                                                $attribute_id,
                                                $set_id,
                                                $group_id,
                                                $attribute['sort_order']
                                            );
                                        } else {
                                            $attribute_api_model->attributeAdd($attribute_id, $set_id, $group_id);
                                        }
                                    } catch (\Magento\Framework\Exception\LocalizedException $e) {
                                        $response['API_ERROR']['set'][] = [
                                            'comment' => 'error with linking attribute: ' . $attribute['attribute_code'] . ' ' . $e->getMessage()
                                        ];
                                    } catch (\Exception $e) {
                                        $response['API_ERROR']['set'][] = [
                                            'comment' => 'error with linking attribute: ' . $attribute['attribute_code'] . ' ' . $e->getMessage()
                                        ];
                                    }
                                }

                                //save all options
                                if (!empty($attribute_id) && !empty($attribute['options'])) {
                                    foreach ($attribute['options'] as $option) {
                                        try {
                                            if (empty($option['magento_id'])) {
                                                $option_id = $attribute_api_model->addOption($attribute_id, $option);

                                                $response['set']['groups'][$group_id]['attributes']
                                                [$attribute['external_id']]['options'][] = [
                                                    'comment' => 'Option added successfully',
                                                    'item' => [
                                                        'value' => $option_id,
                                                        'external_id' => $option['external_id'],
                                                    ]
                                                ];
                                            } else {
                                                $option_id = $option['magento_id'];
                                                $attribute_api_model->updateOption($attribute_id, $option_id, $option);

                                                $response['set']['groups'][$group_id]['attributes']
                                                [$attribute['external_id']]['options'][] = [
                                                    'comment' => 'Option updated successfully',
                                                    'item' => [
                                                        'value' => $option_id,
                                                        'external_id' => $option['external_id'],
                                                    ]
                                                ];
                                            }
                                        } catch (\Magento\Framework\Exception\LocalizedException $e) {
                                            $response['API_ERROR']['set'][] = [
                                                'comment' => 'error with option: ' . $e->getMessage(),
                                                'item' => $option
                                            ];
                                        } catch (\Exception $e) {
                                            $response['API_ERROR']['set'][] = [
                                                'comment' => 'error with option: ' . $e->getMessage(),
                                                'item' => $option
                                            ];
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            $findAttributes = $this->findUnusedAttributes($set_id);

            $unusedAttributes = array_diff($findAttributes, $attributesLast);
            foreach ($unusedAttributes as $unusedAttribute) {
                try {
                    $attribute_api_model->unassign($set_id, $unusedAttribute);

                    $response['UnassignedAttributes']['Success'][] = $unusedAttribute;
                } catch (\Throwable $e) {
                    $response['UnassignedAttributes']['Error'][] = [$unusedAttribute, $e->getMessage()];
                }
            }

            $deletedGroups = $this->deleteUnusedAttributeGroups($set_id);

            $response['DeletedGroups']['Success'] = $deletedGroups['Success'];
            $response['DeletedGroups']['Error'] = $deletedGroups['Error'];

            return json_encode($response);
        }
        return false;
    }

    private function checkOptions(string $option, $magentoOptions)
    {
        foreach ($magentoOptions as $magentoOption) {
            if ($option === $magentoOption->getLabel()) {
                return $magentoOption;
            }
        }

        return false;
    }

    /**
     * Add attribute group to attribute set
     *
     * @param $attributeSetId
     * @param $groupName
     * @param array $data
     * @return int
     * @throws \Exception
     */
    public function groupAdd($attributeSetId, $groupName, $data = [], $groupCollection = false)
    {
        $groupId = false;
        $groupsInSet = [];

        if (!$groupCollection) {
            $groupCollection = $this->connectDB->objectManager->get(
                '\Magento\Eav\Model\ResourceModel\Entity\Attribute\Group\Collection'
            )
                ->setAttributeSetFilter($attributeSetId)
                ->load();
        }

        foreach ($groupCollection as $group) {
            $groupsInSet[$group->getId()] = $group->getAttributeGroupName();
        }
        // if attribute group with same name already exists in store
        // then we add conversion for it and return id of this group
        // else we save attribute and return group id
        $group_id = array_search($groupName, $groupsInSet);
        if ($group_id) {
            if (!empty($data['external_id'])) {
                $this->connectDB->saveConversions($data['external_id'], $group_id, 'attribute_group');
            }
            $groupId = (int)$group_id;
        } else {
            $group = $this->connectDB->objectManager->create('Magento\Eav\Model\Entity\Attribute\Group');
            $group->setAttributeSetId($attributeSetId)
                ->setAttributeGroupName(
                    $groupName
                );

            if (!empty($data['sort_order'])) {
                $group->setSortOrder((int)$data['sort_order']);
            }

            if ($group->itemExists()) {
                $groupId = $group->getId();
            } else {
                try {
                    $group->save();
                    if (!empty($data['external_id'])) {
                        $this->connectDB->saveConversions($data['external_id'], $group->getId(), 'attribute_group');
                    }
                    $groupId = (int)$group->getId();
                } catch (\Exception $e) {
                    $groupId = 'Error while save group name, message: ' . $e->getMessage();
                }
            }
        }
        return $groupId;
    }

    public function saveProductsBatch($data)
    {
        $data = json_decode($data, true);
        $response = [];


        if (!empty($data) && is_array($data)) {
            $products_api_model = $this->connectDB->objectManager->create(
                'Iceshop\Iceshop\Model\ICEpimConnectCatalogProductAttributeApi'
            );
            $products_api_model->connectDB = $this->connectDB;
            $product_entity_type_id = $products_api_model->product_entity_type_id =
                $this->connectDB->objectManager->create(
                    '\Magento\Catalog\Model\Product'
                )->getResource()->getEntityType()->getId();
            $products_api_model->eav_attribute = $this->connectDB->objectManager->create(
                'Magento\Catalog\Model\ResourceModel\Eav\Attribute'
            );
            $products_api_model->eav_model_config = $this->connectDB->objectManager->get(
                '\Magento\Eav\Model\Config'
            );
            $products_api_model->eav_entity_attribute_table = $this->connectDB->resource->getTableName(
                'eav_entity_attribute'
            );
            $products_api_model->catalog_product_website_table = $this->connectDB->resource->getTableName(
                'catalog_product_website'
            );
            $products_api_model->eav_attribute_table = $this->connectDB->resource->getTableName('eav_attribute');
            $products_api_model->catalog_eav_attribute_table = $this->connectDB->resource->getTableName(
                'catalog_eav_attribute'
            );
            $products_api_model->catalog_product_entity_table = $this->connectDB->resource->getTableName(
                'catalog_product_entity'
            );

            $products_api_model->modelProductApi = $this->connectDB->objectManager
                ->create('\Magento\Catalog\Model\Product');
            $products_api_model->storeManagerInterface = $this->connectDB->objectManager
                ->create('\Magento\Store\Model\StoreManagerInterface');

            $products_api_model->action = $this->connectDB->objectManager->create(
                '\Magento\Catalog\Model\Product\Action'
            );

            $select_attributes = $this->connectDB->connection->query("
                    SELECT ea.attribute_code
                    FROM $products_api_model->eav_attribute_table ea
                    RIGHT JOIN $products_api_model->eav_entity_attribute_table eea ON ea.attribute_id = eea.attribute_id
                    WHERE ea.entity_type_id = $product_entity_type_id
                    AND ea.source_model IS NULL GROUP BY ea.attribute_id;
                ");

            $products_api_model->attributes = $select_attributes->fetchAll(\PDO::FETCH_COLUMN, 'attribute_code');

            $catalog_product_entity_int_table = $this->connectDB->resource->getTableName(
                'catalog_product_entity_int'
            );

            $select_visibility = $this->connectDB->connection->query("
                SELECT cpi.entity_id, cpi.value FROM {$catalog_product_entity_int_table} cpi
                JOIN {$products_api_model->eav_attribute_table} ea ON cpi.attribute_id = ea.attribute_id
                WHERE ea.attribute_code = 'visibility' AND cpi.store_id = 0;
            ");

            $products_api_model->visibilities = $select_visibility->fetchAll(\PDO::FETCH_KEY_PAIR);

            foreach ($data as $product) {

                try {
                    if (array_key_exists('product', $product) && !empty($product['product'])) {

                        foreach ($product['product'] as $store_id => $product_data) {

                            if ($product_data && !empty($product_data)) {
                                $products_api_model->update(
                                    $product_data['product_id'],
                                    $product_data['attribute_data'],
                                    $store_id
                                );

                                $response[$product['product_id']][] = [
                                    'comment' => 'Product updated successfully',
                                    'product_id' => $product_data['product_id'],
                                    'store_id' => $store_id
                                ];
                            }
                        }

                        try {
                            $this->connectDB->connection->query("UPDATE `$this->catalog_product_entity_table`
                                SET `updated_ice` = NOW() WHERE `entity_id` = " . $product['product_id']);
                        } catch (\Exception $e) {
                            $response['API_ERROR'][$product['product_id']] = [
                                'comment' => $e->getMessage(),
                                'exception' => [
                                    'class' => get_class($e),
                                    'file' => $e->getFile(),
                                    'line' => $e->getLine()
                                ],
                                'status' => 'Error while updating the product',
                                'item' => $product['product_id'],
                            ];
                        }
                    }

                    if (isset($product['links']) && is_array($product['links'])) {
                        if (isset($product['links']['related']) && is_array($product['links']['related'])) {
                            try {
                                $result = $this->_saveProductsLinks(
                                    $product['product_id'],
                                    $product['links']['related'],
                                    'cross_sell'
                                );
                                $response[$product['product_id']]['cross_sell'] = $result;
                            } catch (\Exception $e) {
                                $response['API_ERROR'][$product['product_id']]['cross_sell'] = [
                                    'comment' => $e->getMessage(),
                                    'status' => 'Error while saving "cross sell" links',
                                    'product_id' => $product['product_id']
                                ];
                            }
                        }

                        if (isset($product['links']['preferred']) && is_array($product['links']['preferred'])) {
                            try {
                                $result = $this->_saveProductsLinks(
                                    $product['product_id'],
                                    $product['links']['preferred'],
                                    'related'
                                );
                                $response[$product['product_id']]['related'] = $result;
                            } catch (\Exception $e) {
                                $response['API_ERROR'][$product['product_id']][] = [
                                    'comment' => $e->getMessage(),
                                    'status' => 'Error while saving "related" links',
                                    'product_id' => $product['product_id']
                                ];
                            }
                        }

                        if (isset($product['links']['alternatives']) && is_array($product['links']['alternatives'])) {
                            try {
                                $result = $this->_saveProductsLinks(
                                    $product['product_id'],
                                    $product['links']['alternatives'],
                                    'up_sell'
                                );
                                $response[$product['product_id']]['up_sell'] = $result;
                            } catch (\Exception $e) {
                                $response['API_ERROR'][$product['product_id']][] = [
                                    'comment' => $e->getMessage(),
                                    'status' => 'Error while saving "up sell" links',
                                ];
                            }
                        }
                    }
                } catch (\Exception $e) {
                    $response['API_ERROR'][$product['product_id']] = [
                        'comment' => $e->getMessage(),
                        'exception' => [
                            'class' => get_class($e),
                            'file' => $e->getFile(),
                            'line' => $e->getLine()
                        ],
                        'status' => 'Error while updating the product',
                        'item' => $product['product_id'],
                    ];
                }
            }
        }

        return json_encode($response);
    }

    /**
     * Working with products links - deleting and adding if needed
     */
    public function _saveProductsLinks($product_id, $relation_arr, $relation_type)
    {
        $response = [];
        $relation_types = [
            'related',
            'up_sell',
            'cross_sell'
        ];

        if (!empty($product_id) && is_array($relation_arr) && in_array($relation_type, $relation_types)) {
            $product_link_api_model = $this->connectDB->objectManager->create(
                'Iceshop\Icepimconnect\Model\ICEpimConnectCatalogProductLink'
            );
            try {
                $links_raw = $product_link_api_model->items($relation_type, $product_id, null, true);
                $links = [];
                foreach ($links_raw as $link_id => $link_item) {
                    $links[] = $link_item[0];
                }
                $links_to_add = array_diff($relation_arr, $links);
                $links_to_delete = array_diff($links, $relation_arr);

                if (!empty($links_to_delete)) {
                    $link = implode(',', $links_to_delete);
                    try {
                        $product_link_api_model->remove($relation_type, $product_id, $links_to_delete);
                        $response[] = [
                            'comment' => 'Link deleted successfully',
                            'item' => [
                                'product_id' => $product_id,
                                'related_product_id' => $link
                            ]
                        ];
                    } catch (\Exception $e) {
                        $response['API_ERROR'][] = [
                            'comment' => $e->getMessage(),
                            'status' => 'Error while deleting link item',
                            'item' => [
                                'product_id' => $product_id,
                                'related_product_id' => $link
                            ]
                        ];
                    }
                }

                if (!empty($links_to_add)) {
                    $link_data = ['is_icecat' => '1'];
                    $link = implode(',', $links_to_add);
                    try {
                        $product_link_api_model->assign($relation_type, $product_id, $links_to_add, $link_data);
                        $response[] = [
                            'comment' => 'Link added successfully',
                            'item' => [
                                'product_id' => $product_id,
                                'related_product_id' => $link
                            ]
                        ];
                    } catch (\Exception $e) {
                        $response['API_ERROR'][] = [
                            'comment' => $e->getMessage(),
                            'status' => 'Error while adding link item',
                            'item' => [
                                'product_id' => $product_id,
                                'related_product_id' => $link
                            ]
                        ];
                    }
                }
            } catch (\Exception $e) {
                $response['API_ERROR'][] = [
                    'comment' => $e->getMessage(),
                    'status' => 'Error while fetching links',
                    'item' => $product_id
                ];
            }
        }
        return $response;
    }

    public function queueProductsImages($data)
    {
        ini_set('memory_limit', -1);
        ini_set('max_execution_time', 0);
        ini_set('post_max_size', '256M');

        $data = json_decode($data, true);

        $response = [];
        if (!empty($data) && is_array($data)) {
            //reset all updated products images
            $updated_products = array_keys($data);
            $fields = [];
            $fields['deleted'] = 1;
            $table_name = $this->connectDB->resource->getTableName('icecat_products_images');
            $where = $this->connectDB->connection->quoteInto('product_id IN (?)', $updated_products);
            try {
                $this->connectDB->connection->update($table_name, $fields, $where);
            } catch (\Exception $e) {
                $response['API_ERROR'] = [
                    'comment' => $e->getMessage(),
                    'status' => 'Error while setting images as deleted'
                ];
                return $response;
            }

            //update images
            foreach ($data as $product) {
                if (empty($product['images'])) {
                    continue;
                }

                try {
                    foreach ($product['images'] as $image) {
//                        $table_name = $this->connectDB->connection->quoteIdentifier('icecat_products_images');
                        if (empty($image['default'])) {
                            $image['default'] = '0';
                        } else {
                            $image['default'] = '1';
                        }
                        $fields = [
                            "product_id" => $product['product_id'],
                            "external_url" => $image['external_url'],
                            "is_default" => $image['default'],
                            "deleted" => 0
                        ];

                        $sql = "INSERT INTO {$table_name} (product_id, external_url, is_default)
                                VALUES (
                                    '" . $fields['product_id'] . "',
                                    '" . $fields['external_url'] . "',
                                    '" . $fields['is_default'] . "'
                                )
                                ON DUPLICATE KEY UPDATE
                                internal_url = IF(is_default <> VALUES(is_default) AND VALUES(is_default) != 0, '', internal_url),
                                deleted = '" . $fields['deleted'] . "',
                                is_default = '" . $fields['is_default'] . "'";

                        $this->connectDB->connection->query($sql);

                        $response[] = [
                            'comment' => 'Added successfully',
                            'item' => $image,
                            'product_id' => $product['product_id']
                        ];
                    }
                } catch (\Exception $e) {
                    $response['API_ERROR'][] = [
                        'comment' => $e->getMessage(),
                        'item' => $image,
                        'product_id' => $product['product_id']
                    ];
                }
            }
        }
        return json_encode($response);
    }

    /**
     * @param $data
     * @return array|bool
     */
    public function updateLanguageCodes($data)
    {
        $data = json_decode($data, true);
        $table_name = $this->connectDB->resource->getTableName('core_config_data');
        if (!empty($data) && !empty($data['language_codes'])) {
            array_unshift(
                $data['language_codes'],
                ['value' => '-1', 'label' => '--' . __('Choose the language') . '--']
            );

            $serializer = ObjectManager::getInstance()->get(\Magento\Framework\Unserialize\Unserialize::class);
            $value = $serializer->serialize($data['language_codes']);
            try {
                $config_id = $this->connectDB->fetchSingleValue(
                    "SELECT `config_id` FROM `" . $table_name . "` WHERE path = :path",
                    [
                        ':path' => 'iceshop_default_icepim_languages'
                    ],
                    'config_id'
                );
                if (isset($config_id)) {
                    $sql = "UPDATE `" . $table_name . "` SET value = '" . $value . "' WHERE config_id = '"
                        . $config_id . "';";
                } else {
                    $sql = "INSERT INTO `" . $table_name . "` (scope, scope_id, path, value)
                            VALUES ('default', '0', 'iceshop_default_icepim_languages', '" . $value . "');";
                }
                $this->connectDB->connection->query($sql);
                return json_encode([
                    'comment' => 'Language codes updated',
                    'data' => json_encode($data)
                ]);
            } catch (\Exception $e) {
                return json_encode([
                    'comment' => $e->getMessage(),
                    'status' => 'Error while saving language codes'
                ]);
            }
        }
        return false;
    }

    /**
     * @param $path
     * @param $data
     * @return bool
     * @throws Exception
     */
    public function _saveConfig($path, $data)
    {
        if (!empty($path) && !empty($data)) {
            $store_config = $this->connectDB->objectManager->create('\Magento\Config\Model\ResourceModel\Config');
            $store_config->saveConfig($path, $data, 'default', 0);
        } else {
            return false;
        }
    }

    /**
     * @return mixed
     * @param mixed $data
     */
    public function setIndexerMode($data)
    {
        $response = ['Status' => 'Error: Invalid Request'];

        $indexingProcesses = $this->connectDB->objectManager->create('Magento\Indexer\Model\Indexer\Collection');
        $indexers = $this->connectDB->objectManager->create('Magento\Indexer\Model\Indexer\State');

        $loadStatuses = [];

        if ($data == 'setWorking') {
            try {
                if (!empty($indexingProcesses)) {
                    $currentState = [];
                    foreach ($indexingProcesses as $process) {
                        $currentState[$process->getId()] = $process->getStatus();
                        $indexers->loadByIndexer($process->getId());
                        $indexers->setStatus('working');
                        $indexers->save();
                    }
                }

                $this->connectDB->setDbVariable('icepimconnect_content_last_start', date('Y-m-d H:i:s'));
            } catch (\Throwable $e) {
                $response = ['Status' => 'Error: ' . $e->getMessage()];
            }

            $response = ['Status' => 'Success'];
        } elseif ($data == 'setValid') {
            try {
                foreach ($indexingProcesses as $process) {
                    $indexers->loadByIndexer($process->getId());
                    $indexers->setStatus('valid');
                    $indexers->save();
                }

                $this->connectDB->setDbVariable('icepimconnect_content_last_finish', date('Y-m-d H:i:s'));
            } catch (\Throwable $e) {
                $response = ['Status' => 'Error: ' . $e->getMessage()];
            }

            $response = ['Status' => 'Success'];
        }

        return json_encode($response);
    }

    /**
     * Run full reindex of shops indexing processes
     *
     * @return string
     */
    public function runFullReindex()
    {
        $this->getConfigWriter();
        $this->connectDB->setDbVariable('icepimconnect_content_last_finish', date('Y-m-d H:i:s'));

        $indexerCollection = $this->_indexerCollectionFactory->create();
        $ids = $indexerCollection->getAllIds();
        try {
            foreach ($ids as $id) {
                $idx = $this->_indexerFactory->create()->load($id);
                $idx->reindexAll($id);
            }
        } catch (\Exception $e) {
            return $e->getMessage();
        }
        return 'Reindex running';
    }

    public function lastInsertId($tableName = null, $primaryKey = null)
    {
        return $this->connectDB->connection->lastInsertId();
    }

    public function reIndex()
    {
        $this->getConfigWriter();
        $this->connectDB->setDbVariable('icepimconnect_content_last_finish', date('Y-m-d H:i:s'));
        $reindexRequired = $this->scopeConfigInterface->getValue(
            'iceshop_Icepimconnect_settings/icepimconnect_service_settings/products_reindex_required');

        if (!$reindexRequired) {
            return json_encode(['reindexRunning' => false]);
        }
        try {
            $this->processor->reindexAll();
        } catch (\Exception $e) {
            return json_encode(['error' => $e->getMessage()]);
        }

        return json_encode(['reindexRunning' => true]);
    }

    /**
     * Start uploading imaages that was added to queue
     *
     * @param mixed $data
     * @return array|string
     */

    public static function checkVersion()
    {
        $productMetadata = ObjectManager::getInstance()->get('Magento\Framework\App\ProductMetadataInterface');
        $version = $productMetadata->getVersion();
        $checkVersion = '';
        if ((string)substr($version, 0, 3) == '2.0') {
            $checkVersion = '2.0';
        }
        return $checkVersion;
    }

    /**
     * Delete products images
     * @param $productId
     * @param $images
     * @param $resources
     */
    protected function cleanMagentoGallery($productId, $images, $resources)
    {
        if ((!empty($productId)) && (!empty($images)) &&
            (!empty($resources['mediaGalleryId'])) && (!empty($this->connectDB->connection))
        ) {

            $imageValueQuery = "SELECT DISTINCT `value_id`
            FROM `{$this->connectDB->resource->getTableName('catalog_product_entity_media_gallery_value')}`
            WHERE `{$this->columnName}` = '{$productId}'";

            $imageValueIds = $this->connectDB->connection->fetchAll($imageValueQuery);
            $mappedImageValueIds = array_map(function ($element) {
                return $element['value_id'];
            }, $imageValueIds);
            $glueImageValueIds = implode(",", $mappedImageValueIds);

            $glueImageValues = implode(",", array_map(function ($item) {
                return "'" . $item . "'";
            }, $images));

            $swatchQuery = $this->connectDB->connection->query("SELECT `value_id`, `value`
                FROM `{$this->connectDB->resource->getTableName('catalog_product_entity_varchar')}`
                WHERE `attribute_id` = '{$resources['swatch_image_attribute_id']}'
                AND `{$this->columnName}` = '{$productId}'
                ;");

            $swatchFetch = $swatchQuery->fetchAll(\PDO::FETCH_KEY_PAIR);
            $gluedSwatch = implode(",", array_map(function ($item) {
                return "'" . $item . "'";
            }, $swatchFetch));

            if (!empty($swatchFetch)) {
                $findSwatchAtQueueQuery = $this->connectDB->connection->query("
                    SELECT `entity_id`, `internal_url`
                    FROM `{$this->connectDB->resource->getTableName('icecat_products_images')}`
                    WHERE internal_url IN ({$gluedSwatch})
                    AND product_id = '{$productId}'
                    ;");
                $fetchExistingImages = $findSwatchAtQueueQuery->fetchAll(\PDO::FETCH_KEY_PAIR);

                $diffOfSwatch = array_diff($swatchFetch, $fetchExistingImages);

                $glueImg = implode(",", array_map(function ($item) {
                    return "'" . $item . "'";
                }, $diffOfSwatch));

                if (!empty($diffOfSwatch)) {
                    $this->connectDB->connection->query("DELETE FROM
                            {$this->connectDB->resource->getTableName('catalog_product_entity_varchar')}
                            WHERE `attribute_id` = '{$resources['swatch_image_attribute_id']}'
                            AND `{$this->columnName}` = '{$productId}'
                            AND `value` IN ($glueImg)
                        ;");

                    $this->connectDB->connection->query("DELETE FROM
                        {$this->connectDB->resource->getTableName('catalog_product_entity_media_gallery')}
                        WHERE `attribute_id` = '{$resources['mediaGalleryId']}'
                        AND `value` IN ({$glueImg})
                      ;");
                }
            }

            if ((!empty($glueImageValues)) && (!empty($glueImageValueIds))) {
                $deleteImageQuery = " DELETE
            FROM `{$this->connectDB->resource->getTableName('catalog_product_entity_media_gallery')}`
            WHERE `attribute_id` = '{$resources['mediaGalleryId']}'
            AND `value` IN ({$glueImageValues})
            AND `value_id` IN ({$glueImageValueIds});";

                $this->connectDB->connection->query($deleteImageQuery);

                foreach ($images as $image) {
                    $pathToImage = $resources['media_folder'] .
                        DIRECTORY_SEPARATOR . 'catalog' .
                        DIRECTORY_SEPARATOR . 'product' .
                        $image;

                    if (file_exists($pathToImage)) {
                        unlink($pathToImage);
                    }
                }
            }
        }
    }

    /**
     * @return mixed
     */
    public function cleanUnusedImages()
    {
        $filesize = 0;
        $count = 0;

        $magentoEntityMediaGalleryTable = $this->connectDB->resource->getTableName('catalog_product_entity_media_gallery');
        $mediaGallery = $this->connectDB->connection->query("SELECT `value` FROM `{$magentoEntityMediaGalleryTable}`");
        $mediaValues = $mediaGallery->fetchAll(\PDO::FETCH_COLUMN);

        $mediaDirectory = $this->filesystem->getDirectoryRead(DirectoryList::MEDIA);
        $imageDirectory = rtrim($mediaDirectory->getAbsolutePath(), "/") . DIRECTORY_SEPARATOR . "catalog" . DIRECTORY_SEPARATOR . "product";
        $directoryIterator = new \RecursiveDirectoryIterator($imageDirectory, \FilesystemIterator::SKIP_DOTS);

        foreach (new \RecursiveIteratorIterator($directoryIterator) as $file) {
            if (
                strpos($file, "/.DS_Store") !== false ||
                strpos($file, "/cache") !== false ||
                strpos($file, "/default") !== false ||
                strpos($file, "/placeholder") !== false ||
                strpos($file, "/watermark") !== false ||
                strpos($file, "/icecatconnect") !== false ||
                is_dir($file)
            ) {
                continue;
            }

            $filePath = str_replace($imageDirectory, '', $file);
            $fullPath = $file->getRealPath();
            if (empty($filePath)) {
                continue;
            }

            if (!in_array($filePath, $mediaValues)) {
                $filesize += filesize($file);
                $count++;

                if (is_file($fullPath)) {
                    if (unlink($fullPath)) {
                        $deleted[] = $fullPath;
                    }
                }
            }
        }

        $response['filesize'] = $this->humansize($filesize);
        $response['count'] = $count;
        $response['files'] = $deleted ?? [];

        return json_encode($response);
    }

    public function humansize($filesize)
    {
        if ($filesize === 0) {
            return 0;
        }

        $base = log($filesize) / log(1024);
        $suffix = array("B", "KB", "MB", "GB", "TB");
        $fbase = floor($base);

        return round(pow(1024, $base - floor($base)), 1) . $suffix[$fbase];
    }

    /**
     * @return mixed
     */
    public function fixDefaultImages()
    {
        $counter = 0;

        // Used tables
        $icecatImagesMappingTable = $this->connectDB->resource->getTableName('icecat_products_images');
        $magentoEntityMediaGalleryTable = $this->connectDB->resource->getTableName('catalog_product_entity_media_gallery');
        $magentoAttributeTable = $this->connectDB->resource->getTableName('eav_attribute');
        $magentoEntityVarcharTable = $this->connectDB->resource->getTableName('catalog_product_entity_varchar');

        // Find image attributes (base, small_image and thumbnail)
        $imageAttributeData = [];

        $findImageAttributes = $this->connectDB->connection->query("SELECT `attribute_id`, `attribute_code` FROM `{$magentoAttributeTable}` WHERE `attribute_code` IN ('thumbnail', 'image', 'small_image')");
        $imageAttributes = $findImageAttributes->fetchAll();

        foreach ($imageAttributes as $imageAttribute) {
            $imageAttributeData[$imageAttribute['attribute_code']] = $imageAttribute['attribute_id'];
        }

        $implodeAttributeIds = implode(',', $imageAttributeData);

        // Find products have no images
        $getProducts = $this->connectDB->connection->query("SELECT `{$this->columnName}`FROM `{$magentoEntityVarcharTable}` WHERE `value` = 'no_selection' AND attribute_id IN ({$implodeAttributeIds}) GROUP BY `{$this->columnName}`");

        foreach ($getProducts as $getProduct) {
            // Used variables
            $founded = 0;

            $productId = $getProduct[$this->columnName];

            // Find product default image in mapping table
            $imagesFromMappingTable = $this->connectDB->connection->query("SELECT `internal_url` FROM `{$icecatImagesMappingTable}` WHERE `product_id` = {$productId} AND `is_default` = 1 AND `internal_url` != ''");

            $defaultImage = $imagesFromMappingTable->fetchAll(\PDO::FETCH_COLUMN, 0);

            // If there is no default image, continue
            if (!$defaultImage) {
                continue;
            }

            $defaultImageFileName = pathinfo($defaultImage[0], PATHINFO_FILENAME);

            // Load product
            $product = $this->repository->getById($productId, true, 0, true);
            $mediaGalleryEntries = $product->getMediaGalleryEntries();

            // Loop media gallery records
            foreach ($mediaGalleryEntries as $mediaGalleryEntry) {
                $filePath = $mediaGalleryEntry->getFile();

                $fileBaseName =  pathinfo($filePath, PATHINFO_FILENAME);
                $fileExtension = pathinfo($filePath, PATHINFO_EXTENSION);

                // If filename or file extension is empty, continue
                if (!$fileExtension || !$fileBaseName) {
                    continue;
                }

                $fileBaseNameCorrection = current(explode('_', $fileBaseName));

                // If found the image in media gallery, break
                if ($fileBaseNameCorrection === $defaultImageFileName) {
                    $founded = 1;
                    break;
                }
            }

            // Start products' image attributes fix
            if ($founded === 1) {
                foreach ($imageAttributeData as $attributeId) {
                    try {
                        $this->connectDB->connection->query("UPDATE `{$magentoEntityVarcharTable}` SET `value` = '{$filePath}' WHERE `attribute_id` = {$attributeId} AND `{$this->columnName}` = {$productId} AND `value` = 'no_selection'");
                    } catch (\Exception $e) {
                        continue;
                    }
                }

                $counter++;
            } else {
                $this->connectDB->connection->query("UPDATE `{$icecatImagesMappingTable}` SET `internal_url` = '' WHERE `product_id` = {$productId} AND `is_default` = 1");
            }
        }

        return json_encode(['counter' => $counter]);
    }

    public function processProductsImagesQueue($data, $broken = 0)
    {
        ini_set('memory_limit', -1);
        ini_set('max_execution_time', 0);
        ini_set('post_max_size', '256M');

        $response = [];

        $select_stores = $this->connectDB->connection->query("
            SELECT store_id FROM {$this->connectDB->resource->getTableName('store')};"
        );

        $this->stores = $select_stores->fetchAll(\PDO::FETCH_COLUMN, 0);


        $data = json_decode($data, true);

        $batch_size = !empty($data) ? ceil($data) : 10;

        $image_counter = 0;

        $imageProcessor = $this->connectDB->objectManager->create('\Magento\Catalog\Model\Product\Gallery\Processor');
        $imageTableName = $this->connectDB->resource->getTableName('icecat_products_images');
        $scopeConfig = $this->connectDB->objectManager->get('\Magento\Framework\App\Config\ScopeConfigInterface');
        $replace_images = $scopeConfig->getValue(
            'iceshop_icecatconnect/icecatconnect_service_settings/replace_product_images_by_icecat'
        );

        $eav_attribute_table_name = $this->connectDB->resource->getTableName('eav_attribute');
        $catalog_product_entity_media_gallery = $this->connectDB->resource->getTableName(
            'catalog_product_entity_media_gallery'
        );
        $catalog_product_entity_media_gallery_value = $this->connectDB->resource->getTableName(
            'catalog_product_entity_media_gallery_value'
        );
        $catalog_product_entity_media_gallery_value_to_entity = $this->connectDB->resource->getTableName(
            'catalog_product_entity_media_gallery_value_to_entity'
        );
        $media_gallery_select_attribute_id = $this->connectDB->connection->query(
            "SELECT attribute_id FROM $eav_attribute_table_name WHERE `attribute_code` = 'media_gallery';"
        );
        $media_gallery_id = $media_gallery_select_attribute_id->fetch()['attribute_id'];

        $directory_list = $this->connectDB->objectManager->get(
            '\Magento\Framework\App\Filesystem\DirectoryList'
        );

        $media_folder = $directory_list->getPath('media') .
            '/' . 'catalog' .
            '/' . 'product';
        $connect_folder = '/icecatconnect/';

        $media_path = $media_folder . $connect_folder;

        $swatch_image_attribute_id_fetch = $this->connectDB->connection->query("
            SELECT `attribute_id`
            FROM {$this->connectDB->resource->getTableName('eav_attribute')}
            WHERE attribute_code = 'swatch_image';")
            ->fetch();
        $swatch_image_attribute_id = null;
        if (!empty($swatch_image_attribute_id_fetch['attribute_id'])) {
            $swatch_image_attribute_id = $swatch_image_attribute_id_fetch['attribute_id'];
        }

        $store_ids = implode(',', $this->stores);

        $internal_batch = 120;
        $number_of_iterations = ceil($batch_size / $internal_batch);

        $broken_count = 0;

        $filter_image_size = (bool)$scopeConfig->getValue(
            'iceshop_icecatconnect/icecatconnect_service_settings/filter_image_size'
        );
        $filter_image_size_value = (float)$scopeConfig->getValue(
            'iceshop_icecatconnect/icecatconnect_service_settings/filter_image_size_value'
        );

        $imagesQueryTotal = "SELECT
                                COUNT(`entity_id`) as cnt
                            FROM `{$imageTableName}`
                            WHERE `deleted` = 0
                            AND `broken` = '{$broken}'
                            AND `internal_url` = ''";

        $fetchImagesTotal = $this->connectDB->fetchSingleValue($imagesQueryTotal, [], 'cnt');

        for ($i = 1, $j = 0; $i <= $number_of_iterations; $i++, $j++) {

            $limit = $internal_batch;

            if ($i == 1) {
                if ((($batch_size - $internal_batch * $j) % $internal_batch != 0)) {
                    $limit = $batch_size % $internal_batch;
                }
            }

            $successDownloaded = [];

            $imagesQuery = $this->connectDB->connection->query("
                SELECT `entity_id`,
                `product_id`,
                `external_url` as `url`,
                SUBSTRING_INDEX(external_url,'/',-1) as `file_name`,
                `internal_url`,
                `is_default`
                FROM `{$imageTableName}`
                WHERE `deleted` = 0
                AND `broken` = '{$broken}'
                AND `internal_url` = ''
                ORDER BY `is_default` DESC, `entity_id` ASC
                LIMIT {$limit};"
            );

            $fetchImages = $imagesQuery->fetchAll();
            $arProdIds = [];

            if (!empty($fetchImages)) {

                // initialization of multi curl

                $chs = [];
                $cmh = curl_multi_init();
                if ($broken == 0) {
                    foreach ($fetchImages as $key => $image) {
                        $image_name = $image['file_name'];
                        $prefix_folder_path = $media_path . substr($image_name, 0, 2) . '/';
                        try {
                            if (!file_exists($media_path)) {
                                mkdir($media_path, 0775, true);
                            }
                            if (!file_exists($prefix_folder_path)) {
                                mkdir($prefix_folder_path, 0775, true);
                            }
                            $tmp_file = $prefix_folder_path . $image['file_name'];

                            if (!file_exists($tmp_file)) {

                                $chs[$key] = curl_init();
                                curl_setopt($chs[$key], CURLOPT_URL, $image['url']);
                                curl_setopt($chs[$key], CURLOPT_RETURNTRANSFER, 1);
                                curl_setopt($chs[$key], CURLOPT_SSL_VERIFYHOST, 0);
                                curl_setopt($chs[$key], CURLOPT_CONNECTTIMEOUT, 10);
                                curl_setopt($chs[$key], CURLOPT_TIMEOUT, 30);
                                curl_multi_add_handle($cmh, $chs[$key]);
                            }

                            $image['internal_url'] = $connect_folder . substr($image_name, 0, 2) . '/' . $image['product_id']
                                . $image['file_name'];
                            $successDownloaded[$key] = $image;

                        } catch (\Exception $e) {
                            $fields = [];
                            $fields['broken'] = '1';
                            $where = $this->connectDB->connection->quoteInto('entity_id = ?', $image['entity_id']);

                            $this->connectDB->connection->update($imageTableName, $fields, $where);
                            $response['API_ERROR'][$image['entity_id']][] = [
                                'comment' => 'Image link is broken or file corrupted',
                                'status' => 'Error while downloading image',
                                'message' => $e->getMessage(),
                                'item' => $image
                            ];

                            continue;
                        }
                    }
                } else {
                    foreach ($fetchImages as $key => $image) {
                        $image_name = $image['file_name'];
                        $prefix_folder_path = $media_path . substr($image_name, 0, 2) . '/';

                        try {
                            if (!file_exists($media_path)) {
                                mkdir($media_path, 0775, true);
                            }
                            if (!file_exists($prefix_folder_path)) {
                                mkdir($prefix_folder_path, 0775, true);
                            }
                            $tmp_file = $prefix_folder_path . $image['file_name'];

                            if (!file_exists($tmp_file)) {

                                $chs[$key] = curl_init();
                                curl_setopt($chs[$key], CURLOPT_URL, $image['url']);
                                curl_setopt($chs[$key], CURLOPT_RETURNTRANSFER, 1);
                                curl_setopt($chs[$key], CURLOPT_SSL_VERIFYHOST, 0);
                                curl_multi_add_handle($cmh, $chs[$key]);
                            }

                            $image['internal_url'] = $connect_folder . substr($image_name, 0, 2) . '/' . $image['product_id']
                                . $image['file_name'];
                            $successDownloaded[$key] = $image;

                        } catch (\Exception $e) {
                            $fields = [];
                            $fields['broken'] = '1';
                            $where = $this->connectDB->connection->quoteInto('entity_id = ?', $image['entity_id']);

                            $this->connectDB->connection->update($imageTableName, $fields, $where);
                            $response['API_ERROR'][$image['entity_id']][] = [
                                'comment' => 'Image link is broken or file corrupted',
                                'status' => 'Error while downloading image',
                                'message' => $e->getMessage(),
                                'item' => $image
                            ];

                            continue;
                        }
                    }
                }

                $running = null;
                do {
                    curl_multi_exec($cmh, $running);
                } while ($running > 0);

                $skipped = [];
                foreach ($successDownloaded as $k => $image) {
                    $arProdIds[] = $image['product_id'];
                    if (isset($chs[$k])) {
                        if (curl_getinfo($chs[$k])['http_code'] == '200') {
                            if ($filter_image_size) {
                                $bytesToMB = curl_getinfo($chs[$k])['size_download'] / 1048576; //convert to MB
                                if ($bytesToMB > $filter_image_size_value) {
                                    $skipped[] = $image['entity_id'];
                                    continue;
                                }
                            }
                            file_put_contents($media_folder . $image['internal_url'], curl_multi_getcontent($chs[$k]));
                        } else {
                            unset($successDownloaded[$k]);
                            $fields = [];
                            $fields['broken'] = '1';
                            $where = $this->connectDB->connection->quoteInto('entity_id = ?', $image['entity_id']);
                            $broken_count++;
                            $this->connectDB->connection->update($imageTableName, $fields, $where);
                            $response['API_ERROR'][$image['entity_id']][] = [
                                'comment' => 'Image link is broken or file corrupted',
                                'status' => 'Error while downloading image',
                                'item' => $image
                            ];
                        }

                        curl_multi_remove_handle($cmh, $chs[$k]);
                        curl_close($chs[$k]);
                    }
                }
                if (!empty($skipped)) {
                    $fields = [];
                    $fields['deleted'] = 1;
                    $where = $this->connectDB->connection->quoteInto('entity_id IN (?)', $skipped);
                    $this->connectDB->connection->update($imageTableName, $fields, $where);
                }

                curl_multi_close($cmh);
            }

            $product_entity_type_id = $this->connectDB->objectManager->create(
                '\Magento\Catalog\Model\Product'
            )->getResource()->getEntityType()->getId();

            $select_img_attrs = $this->connectDB->connection->query("
                SELECT attribute_id, attribute_code
                FROM {$this->connectDB->resource->getTableName('eav_attribute')}
                WHERE attribute_code IN ('thumbnail', 'image', 'small_image')
                AND entity_type_id = '$product_entity_type_id';
            ");
            $this->img_attrs = $select_img_attrs->fetchAll(\PDO::FETCH_KEY_PAIR);

            $data = json_decode($data, true);

            $mediaGalleryId = null;

            $mediaGalleryQuery = "
            SELECT `attribute_id`
            FROM {$this->connectDB->resource->getTableName('eav_attribute')}
            WHERE attribute_code = 'media_gallery';";

            $mediaGalleryId = $this->connectDB->connection->fetchOne($mediaGalleryQuery);

            if (!empty($successDownloaded)) {
                $products_cleaned = [];

                $successDownloadedPath = array_map(function ($element) {
                    return $element['internal_url'];
                }, $successDownloaded);

                $arProdIdsStr = implode(',', array_unique($arProdIds));

                $select_images_to_delete = $this->connectDB->connection->query("
                    SELECT cpemav.{$this->columnName}, cpema.value
                    FROM {$this->connectDB->resource->getTableName('catalog_product_entity_media_gallery')} cpema
                    JOIN {$this->connectDB->resource->getTableName('catalog_product_entity_media_gallery_value')} cpemav
                    ON cpema.value_id = cpemav.value_id
                    WHERE cpema.value_id IN (SELECT value_id
                    FROM {$this->connectDB->resource->getTableName('catalog_product_entity_media_gallery_value_to_entity')}
                    WHERE {$this->columnName} IN ($arProdIdsStr)) AND cpemav.store_id IN ($store_ids);
                ");

                $images_to_delete = $select_images_to_delete->fetchAll();

                $images_to_delete_processed = [];
                foreach ($images_to_delete as $image_to_delete) {
                    $images_to_delete_processed[$image_to_delete["{$this->columnName}"]][] = $image_to_delete['value'];
                }

                foreach ($successDownloaded as $image) {
                    $arProdIds[] = $image['product_id'];
                    $product_id = $image['product_id'];

                    $internal_url = $image['internal_url'];

                    $response[$image['entity_id']][] = [
                        'status' => 'Image was created successfully',
                        'item' => $image
                    ];

                    if (!empty($internal_url)) {
                        $fields = [];
                        if (file_exists($media_folder . $internal_url)) {
                            $fields['internal_url'] = $internal_url;
                        }
                        if ($broken == 1) {
                            $fields['broken'] = '0';
                        }
                        $where = $this->connectDB->connection->quoteInto('entity_id = ?', $image['entity_id']);

                        try {
                            $this->connectDB->connection->update($imageTableName, $fields, $where);

                            $response[$image['entity_id']][] = [
                                'status' => 'Image was added successfully',
                                'item' => $image
                            ];
                        } catch (\Exception $e) {
                            $response['API_ERROR'][$image['entity_id']][] = [
                                'comment' => $e->getMessage(),
                                'status' => 'Error while saving data to mapping table',
                                'item' => $image
                            ];

                            continue;
                        }
                    }

                    $entity_id = $image['product_id'];

                    if (file_exists($media_folder . $internal_url)) {

                        $this->_resize($media_folder . $internal_url);

                        $resources = [
                            'mediaGalleryId' => $mediaGalleryId,
                            'media_folder' => $media_folder,
                            'swatch_image_attribute_id' => $swatch_image_attribute_id
                        ];

                        $product = $this->repository->getById($entity_id, true, 0, true);

                        // delete image if already exists
                        $existingMediaGalleryEntries = $product->getMediaGalleryEntries();
                        if(is_array($existingMediaGalleryEntries)) {
                            foreach ($existingMediaGalleryEntries as $key => $entry) {
                                $oldImage = $entry->getFile();
                                $oldImage = explode( "/", $oldImage );
                                $oldImage = end($oldImage);

                                $internalUrl = explode( "/", $internal_url );
                                $internalUrl = end($internalUrl);
                                $internalUrl = explode( ".", $internalUrl );

                                if(strpos($oldImage, $internalUrl[0]) !== false) {
                                    $this->cleanMagentoGallery($image['product_id'], [$entry->getFile()], $resources);
                                    break;
                                }
                            }
                        }

                        // delete which are not in icecat images
                        if ($replace_images == 1) {
                            $icecatImages = $this->connectDB->connection->query("SELECT `entity_id`, `internal_url`
                                FROM {$imageTableName}
                                WHERE `product_id` = '{$image['product_id']}'
                                AND `internal_url` <> ''
                                ;");
                            $fetchIcecatImages = $icecatImages->fetchAll(\PDO::FETCH_KEY_PAIR);

                            $existingMediaGalleryEntries = $product->getMediaGalleryEntries();
                            if(is_array($existingMediaGalleryEntries)) {
                                foreach ($existingMediaGalleryEntries as $key => $entry) {
                                    $prodImage = $entry->getFile();
                                    $isNotIcecatImage = false;

                                    foreach ($fetchIcecatImages as $catImage) {

                                        $catImage = explode( "/", $catImage );
                                        $catImage = end($catImage);
                                        $catImage = explode(".", $catImage);

                                        if (strpos($prodImage, $catImage[0]) !== false) {
                                            $isNotIcecatImage = true;
                                            break;
                                        }
                                    }

                                    if(!$isNotIcecatImage){
                                        $this->cleanMagentoGallery($image['product_id'], [$prodImage], $resources);
                                    }
                                }
                            }
                        }

                        $types = ['images'];
                        if($image['is_default']) {
                            $types = ['small_image', 'thumbnail', 'image'];
                        }

                        try {
                            $product->addImageToMediaGallery($media_folder.$internal_url, $types, true, false);
                            $product->setStoreId(0);
                            $this->repository->save($product);
                            $image_counter++;
                        } catch (\Throwable $exception) {
                            $response['API_ERROR'][$image['entity_id']][] = [
                                'comment' => $exception->getMessage(),
                                'status' => 'Error while saving image to product',
                                'item' => $image
                            ];

                            try {
                                $fields['internal_url'] = '';
                                $fields['broken'] = 1;
                                $where = $this->connectDB->connection->quoteInto('entity_id = ?', $image['entity_id']);
                                $this->connectDB->connection->update($imageTableName, $fields, $where);
                            } catch (\Exception $exception) {
                                $response['API_ERROR'][$image['entity_id']][] = [
                                    'comment' => $exception->getMessage(),
                                    'status' => 'Error while saving data to mapping table',
                                    'item' => $image
                                ];

                                continue;
                            }

                            continue;
                        }
                    }
                }
            }
        }

        if ($broken == 1) {
            $imagesQueryDeleted = $this->connectDB->connection->query("SELECT *  FROM `{$imageTableName}` WHERE `deleted` = 1");
            $fetchImagesDeleted = count($imagesQueryDeleted->fetchAll() ?? []);
            if ($fetchImagesDeleted > 0) {
                $pagination = ceil($fetchImagesDeleted / $batch_size);

                for ($p = 0; $p < $pagination; $p++) {
                    $this->deleteImages($batch_size);
                }
            }
        }

        $response['image_counter'] = $image_counter;
        $response['fetch_images'] = count($fetchImages ?? []);
        $response['fetch_images_total'] = $fetchImagesTotal;
        return json_encode($response);
    }

    /**
     * Try upload broken images (that have flag  broken = 1)
     * @param mixed $data
     * @return array|string
     */
    public function processBrokenProductsImages($data)
    {
        $broken = 1;
        return $this->processProductsImagesQueue($data, $broken);
    }

    public function fixFailedAttributes()
    {
        $response = [];

        $sql = "
        DELETE
        e.*
        FROM { $this->connectDB->resource->getTableName('catalog_product_entity_int')} e
        INNER JOIN { $this->connectDB->resource->getTableName('eav_attribute')} a
        ON e.`attribute_id` = a.`attribute_id` AND attribute_code LIKE 'i\_%\_1'
        INNER JOIN { $this->connectDB->resource->getTableName('eav_attribute_option')} o
        ON e.`value` = o.`option_id`
        WHERE o.`attribute_id` <> a.`attribute_id`;";
        try {
            $this->connectDB->connection->query($sql);
            $response['success'] = 1;
        } catch (\Exception $e) {
            $response['API_ERROR'] = [
                'comment' => $e->getMessage(),
                'status' => 'Error during cleanup of failed attributes.'
            ];
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $response['API_ERROR'] = [
                'comment' => $e->getMessage(),
                'status' => 'Error during cleanup of failed attributes.'
            ];
        }
        return json_encode($response);
    }

    public function filtersMap($key = false, $value = false, $get = true)
    {
        if (!empty($key)) {
            if ($get == true) {
                return !empty($this->arFiltersMap[$key]) ? $this->arFiltersMap[$key] : false;
            } else {
                $this->arFiltersMap[$key] = $value;
            }
        } else {
            return $this->arFiltersMap;
        }
    }

    /**
     * Parse filters and format them to be applicable for collection filtration
     *
     * @param null|object|array $filters
     * @param array $fieldsMap Map of field names in format: array('field_name_in_filter' => 'field_name_in_db')
     * @return array
     */
    public function parseFilters($filters, $fieldsMap = null)
    {
        // if filters are used in SOAP they must be represented in array format to be used for collection filtration
        if (is_object($filters)) {
            $parsedFilters = [];
            // parse simple filter
            if (isset($filters->filter) && is_array($filters->filter)) {
                foreach ($filters->filter as $field => $value) {
                    if (is_object($value) && isset($value->key) && isset($value->value)) {
                        $parsedFilters[$value->key] = $value->value;
                    } else {
                        $parsedFilters[$field] = $value;
                    }
                }
            }
            // parse complex filter
            if (isset($filters->complex_filter) && is_array($filters->complex_filter)) {
                $parsedFilters += $this->_parseComplexFilter($filters->complex_filter);
            }

            $filters = $parsedFilters;
        }
        // make sure that method result is always array
        if (!is_array($filters)) {
            $filters = [];
        }
        // apply fields mapping
        if (isset($fieldsMap) && is_array($fieldsMap)) {
            foreach ($filters as $field => $value) {
                if (isset($fieldsMap[$field])) {
                    unset($filters[$field]);
                    $field = $fieldsMap[$field];
                    $filters[$field] = $value;
                }
            }
        }
        return $filters;
    }

    /**
     * Parses complex filter, which may contain several nodes, e.g. when user want to fetch orders which were updated
     * between two dates.
     *
     * @param array $complexFilter
     * @return array
     */
    public function _parseComplexFilter($complexFilter)
    {
        $parsedFilters = [];

        foreach ($complexFilter as $filter) {
            if (!isset($filter->key) || !isset($filter->value)) {
                continue;
            }

            list($fieldName, $condition) = [$filter->key, $filter->value];
            $conditionName = $condition->key;
            $conditionValue = $condition->value;
            $this->formatFilterConditionValue($conditionName, $conditionValue);

            if (array_key_exists($fieldName, $parsedFilters)) {
                $parsedFilters[$fieldName] += [$conditionName => $conditionValue];
            } else {
                $parsedFilters[$fieldName] = [$conditionName => $conditionValue];
            }
        }

        return $parsedFilters;
    }

    /**
     * Convert condition value from the string into the array
     * for the condition operators that require value to be an array.
     * Condition value is changed by reference
     *
     * @param string $conditionOperator
     * @param string $conditionValue
     */
    public function formatFilterConditionValue($conditionOperator, &$conditionValue)
    {
        if (is_string($conditionOperator) && in_array($conditionOperator, ['in', 'nin', 'finset'])
            && is_string($conditionValue)
        ) {
            $delimiter = ',';
            $conditionValue = explode($delimiter, $conditionValue);
        }
    }

    /**
     * @return array
     */
    public function processDefaultProductsImages($data)
    {
        ini_set('memory_limit', -1);
        ini_set('max_execution_time', 0);
        ini_set('post_max_size', '256M');

        $data = json_encode($data, true);

        $response = [];

        $table_name = $this->connectDB->resource->getTableName('catalog_product_entity');
        $optionId = false;
        $eav = $this->connectDB->objectManager->get('\Magento\Eav\Model\Config');

        $attribute = $eav->getAttribute('catalog_product', 'active_ice')->getData();
        if (isset($attribute['attribute_id'])) {
            $options = $eav->getAttribute('catalog_product', 'active_ice')->getSource()->getAllOptions();
            foreach ($options as $option) {
                if ($option['label'] == 'Yes') {
                    $optionId = $option['value'];
                    break;
                }
            }
        }

        $select = $this->connectDB->connection
            ->select()
            ->from('icecat_products_images', '*')
            ->where($this->connectDB->connection->quoteInto('deleted=?', '0'))
            ->where($this->connectDB->connection->quoteInto('is_default=?', '1'))
            ->where($this->connectDB->connection->quoteInto('broken=?', '0'))
            ->where($this->connectDB->connection->quoteInto('internal_url<>?', ''));
        if ($optionId !== false) {
            $select->joinInner(['t1' => $table_name], "icecat_products_images.product_id = t1.entity_id", [])
                ->where("t1.active_ice = '" . $optionId . "'");
        }

        if (!empty($data['page_size'])) {
            if (empty($data['page'])) {
                $data['page'] = 1;
            }
            $select->limitPage($data['page'], $data['page_size']);
        }

        $query = $select->query();

        try {

            $default_images = [];
            while ($row = $query->fetch()) {
                $default_images[] = $row;
            }
        } catch (\Exception $e) {

            $response['API_ERROR'] = [
                'comment' => $e->getMessage(),
                'status' => 'Error while fetching default images'
            ];
        }

        if (!empty($default_images)) {
            foreach ($default_images as $image) {
                try {
                    $response[$image['entity_id']][] = [
                        'status' => 'Default image updated',
                        'item' => $image
                    ];
                } catch (\Exception $e) {
                    $response['API_ERROR'][$image['entity_id']][] = [
                        'comment' => 'Error with update default image (' . $e->getMessage() . ')',
                        'status' => 'Error while updating default image',
                        'item' => $image
                    ];
                }
                unset($image);
            }
        }
        unset($default_images, $read_resource, $image, $select);
        return json_encode($response);
    }

    /**
     * Import attributes GTIN or MPN/Brand in to Magento shop
     *
     * @param mixed $data
     * @return string
     * @throws \Exception
     */
    public function saveMainAttributesBatch($data)
    {

        $store_obj = $this->connectDB->objectManager->create(
            '\Magento\Store\Model\StoreManagerInterface'
        );

        $allStores = [];
        $allStores[] = 0; //default store
        foreach ($store_obj->getStores() as $str) {
            $allStores[] = $str->getId();
        }

        $response = [];
        $data = json_decode($data, true);
        if (!empty($data) && is_array($data)) {
            $main_attributes_mapping = $this->getICEShopMapping();
            $products_api_model = $this->connectDB->objectManager->create('Magento\Catalog\Model\Product');
            foreach ($data as $product) {
                if (!array_key_exists('magento_id', $product)) {
                    continue;
                }
                $product_obj = $products_api_model->load($product['magento_id']);
                $type_flag = false;
                if (array_key_exists('ean', $product)) {
                    if ($main_attributes_mapping['ean'] == '') {
                        continue;
                    }
                    $type_flag = 'ean';
                } elseif (array_key_exists('brand_name', $product) && array_key_exists('mpn', $product)) {
                    if ($main_attributes_mapping['brand_name'] == '' || $main_attributes_mapping['mpn'] == '') {
                        continue;
                    }
                    $type_flag = 'brand_mpn';
                }
                if ($type_flag == false) {
                    continue;
                }
                try {
                    switch ($type_flag) {
                        case 'ean':
                            //ean
                            $attribute_code = $main_attributes_mapping['ean'];
                            $product_obj->setData(
                                $attribute_code,
                                $product['ean']
                            );
                            break;

                        case 'brand_mpn':
                            //brand
                            $attribute_code = $main_attributes_mapping['brand_name'];
                            $brand_name_attribute_type = $product_obj->getResource()
                                ->getAttribute($attribute_code)
                                ->getFrontend()
                                ->getInputType();
                            switch ($brand_name_attribute_type) {
                                case 'select':
                                case 'dropdown':
                                    $product_brand_name = $product_obj->getAttributeText($attribute_code);
                                    if (strtolower($product_brand_name) != strtolower($product['brand_name'])) {
                                        $add_option_id = false;
                                        $attribute_ = $this->connectDB->objectManager->create(
                                            'Magento\Eav\Model\Config'
                                        )->getAttribute(
                                            ProductAttributeInterface::ENTITY_TYPE_CODE,
                                            $attribute_code
                                        );
                                        $options = $attribute_->getSource()->getAllOptions();
                                        if (!empty($options)) {
                                            foreach ($options as $key => $value) {
                                                if (!empty($value['label'])) {
                                                    if ($value['label'] == $product['brand_name']) {
                                                        $add_option_id = $value['value'];
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                        if ($add_option_id == false) {
                                            $attr = $this->connectDB->objectManager->create(
                                                '\Magento\Eav\Model\ResourceModel\Entity\Attribute'
                                            );
                                            $attrId = $attr->getIdByCode(
                                                ProductAttributeInterface::ENTITY_TYPE_CODE,
                                                $main_attributes_mapping['brand_name']
                                            );
                                            $option = [];
                                            $option['attribute_id'] = $attrId;
                                            $option['value'][$product['brand_name']][0] = $product['brand_name'];
                                            foreach ($allStores as $store) {
                                                $option['value'][$product['brand_name']][$store] =
                                                    $product['brand_name'];
                                            }
                                            $this->connectDB->objectManager->create(
                                                'Magento\Eav\Setup\EavSetup'
                                            )->addAttributeOption($option);

                                            $add_option_id = false;
                                            $attribute_ = $this->connectDB->objectManager->create(
                                                'Magento\Eav\Model\Config'
                                            )
                                                ->getAttribute(
                                                    ProductAttributeInterface::ENTITY_TYPE_CODE,
                                                    $attribute_code
                                                );
                                            $options = $attribute_->getSource()->getAllOptions();
                                            if (!empty($options)) {
                                                foreach ($options as $key => $value) {
                                                    if (!empty($value['label'])) {
                                                        if ($value['label'] == $product['brand_name']) {
                                                            $add_option_id = $value['value'];
                                                            break;
                                                        }
                                                    }
                                                }
                                            }
                                            if ($add_option_id !== false) {
                                                $product_obj->setData(
                                                    $attribute_code,
                                                    $add_option_id
                                                );
                                            }
                                        } else {
                                            $product_obj->setData(
                                                $attribute_code,
                                                $add_option_id
                                            );
                                        }
                                    }
                                    break;
                                case 'text':
                                    $product_brand_name = $product_obj->getData($attribute_code);
                                    if (empty($product_brand_name)) {
                                        $product_obj->setData(
                                            $attribute_code,
                                            $product['brand_name']
                                        );
                                    }
                                    break;
                            }
                            //mpn
                            $attribute_code = $main_attributes_mapping['mpn'];
                            $product_mpn = $product_obj->getData($attribute_code);
                            if (empty($product_mpn)) {
                                $product_obj->setData(
                                    $attribute_code,
                                    $product['mpn']
                                );
                            } else {
                                if (strtolower($product_mpn) != strtolower($product['mpn'])) {
                                    $product_obj->setData(
                                        $attribute_code,
                                        $product['mpn']
                                    );
                                }
                            }
                            break;
                    }

                    try {
                        if (is_array($errors = $product_obj->validate())) {
                            $strErrors = [];
                            foreach ($errors as $code => $error) {
                                if ($error === true) {
                                    $error = __('Value for "%1" is invalid.', $code)->render();
                                } else {
                                    $error = __('Value for "%1" is invalid: %2', $code, $error)->render();
                                }
                                $strErrors[] = $error;
                            }
                            $this->_fault('data_invalid', implode("\n", $strErrors));
                        }
                        $product_obj->save();
                        $response[]['product'] = $product['product_id'];
                    } catch (\Exception $e) {
                        $response['API_ERROR'][$product['product_id']] = [
                            'comment' => 'data_invalid : (' . $e->getMessage() . ')',
                            'exception' => [
                                'class' => get_class($e),
                                'file' => $e->getFile(),
                                'line' => $e->getLine(),
                                'trace' => $e->getTraceAsString()
                            ]
                        ];
                    }
                } catch (\Exception $e) {
                    $response['API_ERROR'][$product['product_id']] = [
                        'comment' => $e->getMessage(),
                        'exception' => [
                            'class' => get_class($e),
                            'file' => $e->getFile(),
                            'line' => $e->getLine(),
                            'trace' => $e->getTraceAsString()
                        ],
                        'status' => 'Error while updating the product',
                        'product_id' => $product['product_id']
                    ];
                }
            }
        }
        return json_encode($response);
    }

    /**
     * Update attributes labels, sort order
     *
     * @param mixed $data
     * @return string
     * @throws \Exception
     */
    public function attributesRefresh($data)
    {
        $response = [];

        try {
            $source = $data;
            $source = json_decode($source, true);
            $attribute = '';
            if (isset($source['data']) && isset($source['attribute'])) {
                $data = $source['data'];
                $attribute = $source['attribute'];
            } else {
                $response['API_ERROR'] = [
                    'comment' => 'Data not correct',
                ];
            }
            $model = $this->_getAttribute($attribute);
            $entityType = $this->connectDB->objectManager->get(
                '\Magento\Catalog\Model\Product'
            )->getResource()->getEntityType()->getId();
            if ($model->getEntityTypeId() != $entityType) {
                $this->_fault('Entity not same');
            }
            $data['attribute_code'] = $model->getAttributeCode();
            $data['is_user_defined'] = $model->getIsUserDefined();
            $data['frontend_input'] = $model->getFrontendInput();
            $attribute_api_model = $this->connectDB->objectManager->create(
                'Iceshop\Icepimconnect\Model\ICEpimConnectCatalogProductAttributeApi'
            );
            $attribute_api_model->prepareDataForSave($data);
            $model->addData($data);
            try {
                $model->save();
                $response[] = [
                    'comment' => 'Success updated',
                ];
                // clear translation cache because attribute labels are stored in translation
            } catch (\Exception $e) {
                $response['API_ERROR'] = [
                    'comment' => 'unable_to_save (' . $e->getMessage() . ')',
                ];
            }
        } catch (\Exception $e) {
            $response['API_ERROR'] = [
                'comment' => $e->getMessage(),
            ];
        }
        return json_encode($response);
    }

    public function _getAttribute($attribute)
    {
        $model = ObjectManager::getInstance()->create('Magento\Catalog\Model\ResourceModel\Eav\Attribute');

        if (is_numeric($attribute)) {
            $model->load((int)$attribute);
        } else {
            $model->load($attribute, 'attribute_code');
        }

        if (!$model->getId()) {
            $this->_fault('not_exists');
        }

        return $model;
    }

    public function _fault($phrase, $msg = null)
    {
        if (isset($msg)) {
            $phrase = $phrase . '(' . $msg . ')';
        }
        throw new \Exception($phrase);
    }

    /**
     * Remove the images which are not being used.
     * @param int $batch_size
     */
    public function deleteImages($batch_size = 20)
    {
        ini_set('memory_limit', -1);
        ini_set('max_execution_time', 0);
        ini_set('post_max_size', '256M');

        $imageTableName = $this->connectDB->resource->getTableName('icecat_products_images');
        $this->connectDB->connection->query("DELETE FROM `{$imageTableName}` WHERE (`deleted` = 1 OR `broken` = 1) AND `internal_url` = '';");
        $imagesQuery = $this->connectDB->connection->query("
          SELECT *  FROM `{$imageTableName}` WHERE `deleted` = 1 LIMIT {$batch_size};
          ");
        $fetchImages = $imagesQuery->fetchAll();
        foreach ($fetchImages as $fetchImage) {
            $product = $this->connectDB->objectManager->create('Magento\Catalog\Model\Product')->load($fetchImage['product_id']);
            $productRepository = $this->connectDB->objectManager->create('Magento\Catalog\Api\ProductRepositoryInterface');
            $existingMediaGalleryEntries = $product->getMediaGalleryEntries();
            if(is_array($existingMediaGalleryEntries)) {
                foreach ($existingMediaGalleryEntries as $key => $entry) {
                    $imagePath = explode('/', $entry->getFile());
                    $imageName = end($imagePath);
                    $deletedImage = explode('/', $fetchImage['internal_url']);
                    $deletedImage = explode('.', end($deletedImage));

                    if (strpos($imageName, $deletedImage[0]) !== false) {
                        unset($existingMediaGalleryEntries[$key]);
                        break;
                    }
                }

                $product->setMediaGalleryEntries($existingMediaGalleryEntries);
                $productRepository->save($product);
                $this->connectDB->connection->query("DELETE FROM `{$imageTableName}` WHERE `deleted` = 1 AND `entity_id` = {$fetchImage['entity_id']};");
            }
        }
    }

    /**
     * @param $source_url
     */
    public function _resize($source_url)
    {
        $source_url_parts = pathinfo($source_url);
        if (array_key_exists('extension', $source_url_parts)) {
            $extension = $source_url_parts['extension'];
        } else {
            $extension = end(explode('.', $source_url_parts));
        }

        list($width, $height) = getimagesize($source_url);

        $after_width = 1024;

        if ($width > $after_width) {

            if ($extension == 'jpg' || $extension == 'jpeg' || $extension == 'JPG' || $extension == 'JPEG') {
                $img = imagecreatefromjpeg($source_url);
            } elseif ($extension == 'png' || $extension == 'PNG') {
                $img = imagecreatefrompng($source_url);
            } else {
                return;
            }

            $imgResized = imagescale($img, $after_width);

            unlink($source_url);
            imagejpeg($imgResized, $source_url);

            imagedestroy($img);
            imagedestroy($imgResized);
        }
    }

    public function setCategoryIsActive()
    {
        $categoryStatus = $this->scopeConfigInterface->getValue(
            'iceshop_Icepimconnect_settings/icepimconnect_products_mapping/category_status'
        );
        if ($categoryStatus == 1) {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $collectionFactory = $objectManager->create('\Magento\Catalog\Model\ResourceModel\Category\CollectionFactory');

            $collection = $collectionFactory->create();
            $collection->addAttributeToSelect('*');
            $subcats = $collection;
            foreach ($this->storeManager->getStores(true) as $str) {
                $storeId = $str->getId();

                foreach ($subcats as $subcat) {
                    try {
                        $categoryFactory = $this->categoryRepositoryInterface->get($subcat->getId(), $storeId);

                        if (!empty($categoryFactory)) {
                            $this->storeManager->setCurrentStore($storeId);
                            $categoryFactory->setStoreId($storeId);

                            $productCollection = $this->productCollectionFactory->create();
                            $productCollection->addCategoryFilter($categoryFactory);
                            $productCollection->addAttributeToFilter('status', \Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED);
                            $sum = $productCollection->getSize() ?? 0;

                            $isActive = 1;

                            if ($sum == 0 || !$categoryFactory->getIsActive()) {
                                $isActive = 0;
                            }

                            $categoryFactory->setIsActive($isActive);

                            $this->categoryRepositoryInterface->save($categoryFactory);
                        }
                    } catch (\Throwable $e) {
                        continue;
                    }
                }
            }
        }
    }

    /**
     * @return array|mixed
     */
    public function getProductConversions()
    {
        return json_encode($this->connectDB->getProductConversions());
    }

    /**
     * @param mixed $data
     * @return false|string
     */
    public function crudProducts($data)
    {
        ini_set('memory_limit', -1);
        ini_set('max_execution_time', 0);
        ini_set('post_max_size', '256M');

        $icePimProducts = json_decode($data, true);

        $createdProducts = [];

        $deletedProducts = [];

        $websiteLists = $this->storeManager->getWebsites();

        if (!empty($websiteLists)) {
            foreach ($websiteLists as $value) {
                $websiteList[$value->getCode()] = $value->getId();
                $storeList[$value->getCode()] = $this->connectDB->objectManager->create('\Magento\Store\Api\StoreWebsiteRelationInterface')->getStoreByWebsiteId($value->getId());
                $storeListById[$value->getId()] = $this->connectDB->objectManager->create('\Magento\Store\Api\StoreWebsiteRelationInterface')->getStoreByWebsiteId($value->getId());
            }
        }

        $mpn = $this->scopeConfigInterface->getValue(
            'iceshop_Icepimconnect_settings/icepimconnect_products_mapping/products_mapping_mpn'
        );
        $brandName = $this->scopeConfigInterface->getValue(
            'iceshop_Icepimconnect_settings/icepimconnect_products_mapping/products_mapping_brand'
        );
        $ean = $this->scopeConfigInterface->getValue(
            'iceshop_Icepimconnect_settings/icepimconnect_products_mapping/products_mapping_gtin'
        );
        $stateName = $this->scopeConfigInterface->getValue(
            'iceshop_Icepimconnect_settings/icepimconnect_products_mapping/products_mapping_state'
        );
        $icepimconnect = $this->scopeConfigInterface->getValue(
            'iceshop_Icepimconnect_settings/icepimconnect_products_mapping/products_mapping_icepimconnect'
        );
        $websites = $this->scopeConfigInterface->getValue(
            'iceshop_Icepimconnect_settings/icepimconnect_products_mapping/products_websites'
        );
        $urlKeyUpdate = $this->scopeConfigInterface->getValue(
            'iceshop_Icepimconnect_settings/icepimconnect_service_settings/products_url_key_update'
        );
        $urlKeyPrefix = $this->scopeConfigInterface->getValue(
            'iceshop_Icepimconnect_settings/icepimconnect_service_settings/products_url_key_prefix'
        );
        $useInsteadOfSku = $this->scopeConfigInterface->getValue(
            'iceshop_Icepimconnect_settings/icepimconnect_products_mapping/products_use_instead_of_sku'
        );

        $allStores = [];
        $allStores[] = 0; //default store

        if ($websites === "all") {
            $websites = 0;
            foreach ($this->storeManager->getStores() as $str) {
                $allStores[] = $str->getId();
            }
        } else {
            foreach ($storeListById[$websites] as $str) {
                $allStores[] = $str;
            }
        }

        foreach ($icePimProducts as $icePimProductId => $icePimProduct) {
            $categories = [];
            try {
                if (isset($icePimProduct['selectedProductCategories'])) {
                    $categories = explode(';', $icePimProduct['selectedProductCategories']);
                }

                if (!$icePimProduct['deleted']) {
                    $nameForUrlKey = $icePimProduct['name'];
                    $brandForUrlKey = $icePimProduct['brand_name'];
                    $mpnForUrlKey = $icePimProduct['mpn'];

                    if ($urlKeyPrefix == 0) {
                        $urlDef = $nameForUrlKey;
                    } elseif ($urlKeyPrefix == 1) {
                        $urlDef = $mpnForUrlKey . '-' . $nameForUrlKey;
                    } elseif ($urlKeyPrefix == 2) {
                        $urlDef = $brandForUrlKey . '-' . $nameForUrlKey;
                    } elseif ($urlKeyPrefix == 3) {
                        $urlDef = $mpnForUrlKey . '-' . $brandForUrlKey . '-' . $nameForUrlKey;
                    } elseif ($urlKeyPrefix == 4) {
                        $urlDef = $mpnForUrlKey;
                    } elseif ($urlKeyPrefix == 5) {
                        $urlDef = $brandForUrlKey . '-' . $mpnForUrlKey . '-' . $nameForUrlKey;
                    } elseif ($urlKeyPrefix == 6) {
                        $urlDef = $nameForUrlKey . '-' . $mpnForUrlKey;
                    } else {
                        $urlDef = $mpnForUrlKey . '-' . $brandForUrlKey . '-' . $nameForUrlKey;
                    }
                }

                if (!empty($icePimProduct['magentoId'] && !$icePimProduct['deleted'])) {
                    try {
                        $product = $this->repository->getById((int)$icePimProduct['magentoId'], true, 0, true);
                    } catch (\Throwable $exception) {
                        $deletedProducts[] = $icePimProductId;
                        continue;
                    }

                    if (!empty($icePimProduct['name'])) {
                        if ($urlKeyUpdate == 1) {
                            $urlKey = $product->formatUrlKey($urlDef);
                            $product->setUrlKey($urlKey);
                        }

                        $this->productAction->updateAttributes([$icePimProduct['magentoId']], ['name' => $icePimProduct['name']], 0);
                        foreach ($allStores as $str) {
                            if (isset($icePimProduct['names'][$str])) {
                                $this->productAction->updateAttributes([$icePimProduct['magentoId']], ['name' => $icePimProduct['names'][$str]], $str);
                            }
                        }
                    }
                } elseif ($icePimProduct['deleted'] && !empty($icePimProduct['magentoId'])) {
                    try {
                        $product = $this->repository->getById((int)$icePimProduct['magentoId']);

                        $magentoProductId = (int)$product->getId();

                        if ($magentoProductId) {
                            $this->repository->delete($product);
                            $deletedProducts[] = $icePimProductId;
                        } else {
                            $deletedProducts[] = $icePimProductId;
                        }
                        continue;
                    } catch (\Throwable $exception) {
                        $deletedProducts[] = $icePimProductId;
                        continue;
                    }
                }

                if (!$icePimProduct['magentoId']) {
                    $product = $this->productInterfaceFactory->create();
                    $product->setStoreId(0);
                    $product->setName((string)$icePimProduct['name']);

                    $urlKey = $product->formatUrlKey($urlDef);
                    $product->setUrlKey($urlKey);
                }

                if (!isset($icePimProduct['mpn'])) {
                    continue;
                }

                if (!$icePimProduct['name']) {
                    $createdProducts[$icePimProductId] = 'Empty name for product with mpn: ' . $icePimProduct['mpn'] . ' and brand name: ' . $icePimProduct['brand_name'];
                    continue;
                } elseif (!$icePimProduct['sku']) {
                    $createdProducts[$icePimProductId] = 'Empty sku for product with mpn:' . $icePimProduct['mpn'] . ' and brand name: ' . $icePimProduct['brand_name'];
                    continue;
                } elseif (!$icePimProduct['sell_price']) {
                    $createdProducts[$icePimProductId] = 'Empty sell_price for product with mpn:' . $icePimProduct['mpn'] . ' and brand name: ' . $icePimProduct['brand_name'];
                    continue;
                }

                $qty = 0;
                $stockStatus = false;
                if (isset($icePimProduct['qty']) && (int)$icePimProduct['qty'] > 0) {
                    $qty = $icePimProduct['qty'];
                    $stockStatus = true;
                }

                $product->setTypeId('simple');
                if ($websites !== 0 && !empty($icePimProduct['name'])) {
                    $product->setName((string)$icePimProduct['name']);
                }
                $product->setSku($icePimProduct[$useInsteadOfSku] ?? $icePimProduct['sku']);
                $product->setCustomAttribute($mpn, $icePimProduct['mpn']);

                $product->setCustomAttribute($ean, $icePimProduct['ean']);
                if (empty($icePimProduct['magentoId'])) {
                    $product->setAttributeSetId(4);#default atribute set
                }

                $product->setCustomAttribute($icepimconnect, $icePimProductId);
                $product->setPrice((float)$icePimProduct['sell_price']);
                $product->setData('quantity_and_stock_status', ['qty' => $qty, 'is_in_stock' => $stockStatus]);
                $product->setData('cost', $icePimProduct['purchase_price']);

                if (!$icePimProduct['magentoId']) {
                    $product = $this->repository->save($product);
                    $productId = (int)$product->getId();
                } else {
                    $productId = $icePimProduct['magentoId'];
                }

                /******/
                $this->connectDB->saveConversions(
                    $icePimProductId,
                    $productId,
                    'products',
                    'imports_conversions_rules_onboarding'
                );

                $this->connectDB->saveConversions(
                    $icePimProductId,
                    $productId,
                    'products',
                    'imports_conversions_rules_logistics_updated'
                );
                /******/


                $createdProducts[$icePimProductId] = $productId;
                $brand_name_attribute_type = $product->getResource()
                    ->getAttribute($brandName)
                    ->getFrontend()
                    ->getInputType();
                switch ($brand_name_attribute_type) {
                    case 'select':
                    case 'dropdown':
                        $product_brand_name = $product->getAttributeText($brandName);
                        if (strtolower($product_brand_name) != strtolower($icePimProduct['brand_name'])) {
                            $add_option_id = false;
                            $attribute_ = $this->connectDB->objectManager->create(
                                'Magento\Eav\Model\Config'
                            )
                                ->getAttribute(
                                    ProductAttributeInterface::ENTITY_TYPE_CODE,
                                    $brandName
                                );
                            $options = $attribute_->getSource()->getAllOptions();
                            if (!empty($options)) {
                                foreach ($options as $value) {
                                    if (!empty($value['label'])) {
                                        if ($value['label'] == $icePimProduct['brand_name']) {
                                            $add_option_id = $value['value'];
                                            break;
                                        }
                                    }
                                }
                            }
                            if ($add_option_id == false) {
                                $attrId = $this->attribute->getIdByCode(
                                    ProductAttributeInterface::ENTITY_TYPE_CODE,
                                    $brandName
                                );

                                $option = [];
                                $option['attribute_id'] = $attrId;
                                $option['value'][$add_option_id ?? 0][0] = $icePimProduct['brand_name'];

                                $this->eavSetup->addAttributeOption($option);

                                $add_option_id = false;

                                $attribute_ = $this->connectDB->objectManager->create(
                                    'Magento\Eav\Model\Config'
                                )
                                    ->getAttribute(
                                        ProductAttributeInterface::ENTITY_TYPE_CODE,
                                        $brandName
                                    );
                                $options = $attribute_->getSource()->getAllOptions();
                                if (!empty($options)) {
                                    foreach ($options as $key => $value) {
                                        if (!empty($value['label'])) {
                                            if ($value['label'] == $icePimProduct['brand_name']) {
                                                $add_option_id = $value['value'];
                                                break;
                                            }
                                        }
                                    }
                                }
                                if ($add_option_id != false) {
                                    $product->setData(
                                        $brandName,
                                        $add_option_id
                                    );
                                }
                                if (!empty($productId)) {
                                    $this->productAction->updateAttributes([$productId], [$brandName => $add_option_id], 0);
                                    break;
                                }
                            } else {
                                $this->productAction->updateAttributes([$productId], [$brandName => $add_option_id], 0);
                            }
                        }
                        break;
                    case 'text':
                        $product_brand_name = $product->getData($brandName);
                        if (empty($product_brand_name)) {
                            $product->setData(
                                $brandName,
                                $icePimProduct['brand_name']
                            );
                        }
                        break;
                }

                if (!empty($stateName) && !is_null($icePimProduct['state'])) {
                    $state_name_attribute_type = $product->getResource()
                        ->getAttribute($stateName)
                        ->getFrontend()
                        ->getInputType();
                    switch ($state_name_attribute_type) {
                        case 'select':
                        case 'dropdown':
                            $product_state_name = $product->getAttributeText($stateName);
                            if (strtolower($product_state_name) != strtolower($icePimProduct['state'])) {
                                $add_option_id = false;
                                $attribute_ = $this->connectDB->objectManager->create(
                                    'Magento\Eav\Model\Config'
                                )
                                    ->getAttribute(
                                        ProductAttributeInterface::ENTITY_TYPE_CODE,
                                        $stateName
                                    );
                                $options = $attribute_->getSource()->getAllOptions();
                                if (!empty($options)) {
                                    foreach ($options as $value) {
                                        if (!empty($value['label'])) {
                                            if ($value['label'] == $icePimProduct['state']) {
                                                $add_option_id = $value['value'];
                                                break;
                                            }
                                        }
                                    }
                                }
                                if ($add_option_id == false) {
                                    $attrId = $this->attribute->getIdByCode(
                                        ProductAttributeInterface::ENTITY_TYPE_CODE,
                                        $stateName
                                    );

                                    $option = [];
                                    $option['attribute_id'] = $attrId;
                                    $option['value'][$add_option_id ?? 0][0] = $icePimProduct['state'];

                                    $this->eavSetup->addAttributeOption($option);

                                    $add_option_id = false;

                                    $attribute_ = $this->connectDB->objectManager->create(
                                        'Magento\Eav\Model\Config'
                                    )
                                        ->getAttribute(
                                            ProductAttributeInterface::ENTITY_TYPE_CODE,
                                            $stateName
                                        );
                                    $options = $attribute_->getSource()->getAllOptions();
                                    if (!empty($options)) {
                                        foreach ($options as $key => $value) {
                                            if (!empty($value['label'])) {
                                                if ($value['label'] == $icePimProduct['state']) {
                                                    $add_option_id = $value['value'];
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                    if ($add_option_id != false) {
                                        $product->setData(
                                            $stateName,
                                            $add_option_id
                                        );
                                    }
                                    if (!empty($productId)) {
                                        $this->productAction->updateAttributes([$productId], [$stateName => $add_option_id], 0);
                                        break;
                                    }
                                } else {
                                    $this->productAction->updateAttributes([$productId], [$stateName => $add_option_id], 0);
                                }
                            }
                            break;
                        case 'text':
                            $product_state_name = $product->getData($stateName);
                            if (empty($product_state_name)) {
                                $product->setData(
                                    $stateName,
                                    $icePimProduct['state']
                                );
                            }
                            break;
                    }
                }

                if ($icePimProduct['productStatus'] == 'disabled') {
                    $product->setStatus(2);
                } else {
                    $product->setStatus(1);
                }

                if (array_key_exists('totalShop', $icePimProduct)) {
                    $totalShop = (int)$icePimProduct['totalShop'];
                    if ($totalShop === 1) {

                        $websiteLists = $this->storeManager->getWebsites();

                        if (!empty($websiteLists) && $websites == 0) {
                            foreach ($websiteLists as $value) {
                                $websiteIds[] = $value->getId();
                            }

                            $product->setWebsiteIds($websiteIds);
                            unset($websiteIds);

                        } else {
                            $product->setWebsiteIds([$websites]);
                        }

                        $product->setStoreId(0);

                        if ($icePimProduct['productStatus'] == 'disabled') {
                            $product->setStatus(2);
                        } else {
                            $product->setStatus(1);
                        }
                    }
                }

                if (array_key_exists('productShops', $icePimProduct)) {

                    $websiteIds = [];

                    if (!empty($icePimProduct['productShops'])) {
                        $productShops = explode(';', $icePimProduct['productShops']);

                        foreach ($productShops as $productShop) {
                            if (array_key_exists($productShop, $websiteList)) {
                                $websiteIds[] = $websiteList[$productShop];
                            }
                        }

                        $product->setWebsiteIds($websiteIds);

                        $product->setStoreId(0);

                        if ($icePimProduct['productStatus'] == 'disabled' || empty($websiteIds)) {
                            $product->setStatus(2);
                        } else {
                            $product->setStatus(1);
                        }
                    } else {
                        $product->setWebsiteIds($websiteIds);
                        $product->setStatus(2);
                    }
                }

                // Multi Shop Multi Values
                if (array_key_exists('multiShop', $icePimProduct)) {
                    $priceGroupCodes = explode(';', $icePimProduct['priceGroupCodes']);

                    $prices = $icePimProduct['multiShop']['prices'];

                    foreach ($priceGroupCodes as $priceGroupCode) {
                        if (array_key_exists($priceGroupCode, $storeList)) {
                            $sellPrice = $prices[$priceGroupCode][$priceGroupCode . '_' . $icePimProduct['priceSelector']['sellPrice']];
                            $promoPrice = $prices[$priceGroupCode][$priceGroupCode . '_' . $icePimProduct['priceSelector']['promoPrice']];

                            if ($sellPrice === '' || (float)$sellPrice === 0.0000) {
                                $sellPrice = null;
                            }

                            if ($promoPrice === '' || (float)$promoPrice === 0.0000) {
                                $promoPrice = null;
                            }

                            foreach ($storeList[$priceGroupCode] as $store) {
                                $this->productAction->updateAttributes([$productId], ['price' => $sellPrice], (int)$store);
                                $this->productAction->updateAttributes([$productId], ['special_price' => $promoPrice], (int)$store);
                            }
                        }
                    }

                    if (array_key_exists('customFields', $icePimProduct['multiShop'])) {
                        foreach ($icePimProduct['multiShop']['customFields'] as $storeId => $customFields) {
                            foreach ($customFields as $field => $customField) {
                                $splitField = explode('_', $field);
                                $shopCode = current($splitField);
                                $fieldCode = str_replace($shopCode . '_', '', $field);
                                $fieldValue = html_entity_decode(strip_tags($customField));
                                if (array_key_exists($shopCode, $storeList)) {
                                    if (!strstr($field, 'price') && !strstr($field, 'products_code') && !strstr($field, 'publish')) {
                                        $setFieldCode = 'set' . ucfirst($fieldCode);
                                        $product->addAttributeUpdate($fieldCode, $fieldValue, $storeId);
                                        $product->{$setFieldCode}($fieldValue);
                                    }
                                }
                            }
                        }
                    }
                }
                if (array_key_exists('singleShop', $icePimProduct)) {
                    if (array_key_exists('customFields', $icePimProduct['singleShop'])) {
                        foreach ($icePimProduct['singleShop']['customFields'] as $storeId => $customFields) {
                            foreach ($customFields as $field => $customField) {
                                $fieldCode = $field;
                                $fieldValue = html_entity_decode(strip_tags($customField));
                                $setFieldCode = 'set' . ucfirst($fieldCode);
                                try {
                                    $product->addAttributeUpdate($fieldCode, $fieldValue, $storeId);
                                    $product->{$setFieldCode}($fieldValue);
                                } catch (\Throwable $e) {
                                    continue;
                                }
                            }
                        }
                    }
                }

                $this->repository->save($product);

                try {
                    $categoryTableName = $this->connectDB->resource->getTableName('catalog_category_product');
                    $this->connectDB->connection->query("DELETE FROM `{$categoryTableName}` WHERE `product_id` = {$productId}");

                    if (!empty($categories)) {
                        foreach ($categories as $category) {
                            $this->connectDB->connection->query("INSERT INTO `{$categoryTableName}` (`category_id`, `product_id`) VALUES ('{$category}', '{$productId}')");
                        }
                    }
                } catch (\Throwable $e) {
                    $createdProducts[$icePimProductId] = $e->getMessage() . ' Error while category saving: ' . $icePimProduct['sku'];
                    continue;
                }
            } catch (\Throwable $e) {
                if (!isset($icePimProduct['mpn']) || !isset($icePimProduct['brand_name'])) {
                    $createdProducts[$icePimProductId] = $e->getMessage() . ' with pimId: ' . $icePimProductId . ' with mpn: no mpn and brand name: no brand name';

                    if ($icePimProduct['deleted']) {
                        $deletedProducts[] = $icePimProductId;
                    }

                    continue;
                }

                $createdProducts[$icePimProductId] = $e->getMessage() . ' with mpn: ' . $icePimProduct['mpn'] . ' and brand name: ' . $icePimProduct['brand_name'];
                continue;
            }

            $createdProducts[$icePimProductId] = $productId;

            unset($product);
        }

        if (count($deletedProducts) > 0) {
            $this->connectDB->deleteProductsBulk($deletedProducts, 'products');
        }

        return json_encode(['product_ids' => $createdProducts, 'deletedProducts' => $deletedProducts]);
    }

    /**
     * @param mixed $data
     * @return string|void
     */
    public function saveConversionIds($data)
    {
        $response = [];
        $products = json_decode($data, true);

        foreach ($products as $icepimId => $product) {
            try {
                $action = 'imports_conversions_rules_content_updated';
                $update = $product['updated'];
                $magentoId = $product['magentoId'];


                $this->connectDB->saveConversions(
                    $icepimId,
                    $magentoId,
                    'products',
                    $action,
                    $update
                );

                $response[$icepimId] = [
                    'status' => 'Success',
                    'magentoId' => $product['magentoId'],
                ];
            } catch (\Exception $exception) {
                $response[$icepimId] = [
                    'status' => 'Error',
                    'magentoId' => $product['magentoId'],
                ];

                continue;
            }
        }

        return json_encode($response);
    }

    /**
     * @param mixed $data
     * @return string|void
     */
    public function priceAndStockUpdate($data)
    {
        ini_set('memory_limit', -1);
        ini_set('max_execution_time', 0);
        ini_set('post_max_size', '256M');

        $icePimProducts = json_decode($data, true);
        $changedProduct = [];
        $withError = [];

        $stateName = $this->scopeConfigInterface->getValue(
            'iceshop_Icepimconnect_settings/icepimconnect_products_mapping/products_mapping_state'
        );

        $websites = $this->scopeConfigInterface->getValue(
            'iceshop_Icepimconnect_settings/icepimconnect_products_mapping/products_websites'
        );

        if ($websites === "all") {
            $websites = 0;
        }

        $websiteLists = $this->storeManager->getWebsites();

        if (!empty($websiteLists)) {
            foreach ($websiteLists as $value) {
                $websiteList[$value->getCode()] = $value->getId();
                $storeList[$value->getCode()] = $this->connectDB->objectManager->create('\Magento\Store\Api\StoreWebsiteRelationInterface')->getStoreByWebsiteId($value->getId());
            }
        }

        foreach ($icePimProducts as $productId => $icePimProduct) {
            if ($icePimProduct['magentoId'] === '') {
                continue;
            }

            if (array_key_exists('threshold', $icePimProduct)) {
                if ($icePimProduct['threshold'] == 1) {
                    $qty = 0;
                    $stockStatus = 0;

                    try {
                        $product = $this->productRepository->getById((int)$icePimProduct['magentoId']);
                        $product->setData('quantity_and_stock_status', ['qty' => $qty, 'is_in_stock' => $stockStatus]);
                        $this->repository->save($product);

                        $changedProduct[$productId] = $icePimProduct['magentoId'];

                        $this->connectDB->saveConversions(
                            $productId,
                            $icePimProduct['magentoId'],
                            'products',
                            'imports_conversions_rules_logistics_updated'
                        );
                    } catch (\Throwable $exception) {
                        $withError[$productId] = $icePimProduct['magentoId'] . ': ' . $exception->getMessage();
                        continue;
                    }
                }

                continue;
            }

            $qty = 0;
            $stockStatus = 0;
            if ((int)$icePimProduct['qty'] > 0) {
                $qty = (int)$icePimProduct['qty'];
                $stockStatus = 1;
            }

            if ($icePimProduct['promo'] === '' || (float)$icePimProduct['promo'] === 0.0000) {
                $icePimProduct['promo'] = null;
            }

            $bulkInventoryData = [];
            $bulkInventoryData['cost'] = $icePimProduct['purchase_price'];

            try {
                $product = $this->productRepository->getById((int)$icePimProduct['magentoId']);

                if (array_key_exists('multiShop', $icePimProduct)) {
                    $priceGroupCodes = explode(';', $icePimProduct['priceGroupCodes']);

                    $prices = $icePimProduct['multiShop']['prices'];

                    foreach ($priceGroupCodes as $priceGroupCode) {

                        if (array_key_exists($priceGroupCode, $prices)) {
                            $sellPrice = $prices[$priceGroupCode][$priceGroupCode . '_' . $icePimProduct['priceSelector']['sellPrice']];
                            $promoPrice = $prices[$priceGroupCode][$priceGroupCode . '_' . $icePimProduct['priceSelector']['promoPrice']];

                            if ($sellPrice === '' || (float)$sellPrice === 0.0000) {
                                $sellPrice = null;
                            }

                            if ($promoPrice === '' || (float)$promoPrice === 0.0000) {
                                $promoPrice = null;
                            }

                            if (array_key_exists($priceGroupCode, $storeList)) {
                                foreach ($storeList[$priceGroupCode] as $store) {
                                    $bulkInventoryData['price'] = $sellPrice;
                                    $bulkInventoryData['special_price'] = $promoPrice;
                                    $this->productAction->updateAttributes([$icePimProduct['magentoId']], $bulkInventoryData, (int)$store);
                                }
                            }
                            $this->productAction->updateAttributes([$icePimProduct['magentoId']], ['cost' => $icePimProduct['purchase_price']], 0);
                        }
                    }
                } else {
                    if ($icePimProduct['sell_price'] === '' || (float)$icePimProduct['sell_price'] === 0.0000) {
                        $product->setStatus(2);
                        $withError[$productId] = $icePimProduct['magentoId'] . ': Please check the price of the product in the store.';
                    } else {
                        $bulkInventoryData['price'] = $icePimProduct['sell_price'];
                        $bulkInventoryData['special_price'] = $icePimProduct['promo'];
                        $product->setPrice($bulkInventoryData['price']);
                        $product->setSpecialPrice($bulkInventoryData['special_price']);
                        $product->setCost($bulkInventoryData['cost']);
                        $this->productAction->updateAttributes([$icePimProduct['magentoId']], $bulkInventoryData, 0);
                    }
                }

                if (!empty($stateName) && !is_null($icePimProduct['state'])) {
                    $state_name_attribute_type = $product->getResource()
                        ->getAttribute($stateName)
                        ->getFrontend()
                        ->getInputType();
                    switch ($state_name_attribute_type) {
                        case 'select':
                        case 'dropdown':
                            $product_state_name = $product->getAttributeText($stateName);
                            if (strtolower($product_state_name) != strtolower($icePimProduct['state'])) {
                                $add_option_id = false;
                                $attribute_ = $this->connectDB->objectManager->create(
                                    'Magento\Eav\Model\Config'
                                )
                                    ->getAttribute(
                                        ProductAttributeInterface::ENTITY_TYPE_CODE,
                                        $stateName
                                    );
                                $options = $attribute_->getSource()->getAllOptions();
                                if (!empty($options)) {
                                    foreach ($options as $value) {
                                        if (!empty($value['label'])) {
                                            if ($value['label'] == $icePimProduct['state']) {
                                                $add_option_id = $value['value'];
                                                break;
                                            }
                                        }
                                    }
                                }
                                if ($add_option_id == false) {
                                    $attrId = $this->attribute->getIdByCode(
                                        ProductAttributeInterface::ENTITY_TYPE_CODE,
                                        $stateName
                                    );

                                    $option = [];
                                    $option['attribute_id'] = $attrId;
                                    $option['value'][$add_option_id ?? 0][0] = $icePimProduct['state'];

                                    $this->eavSetup->addAttributeOption($option);

                                    $add_option_id = false;

                                    $attribute_ = $this->connectDB->objectManager->create(
                                        'Magento\Eav\Model\Config'
                                    )
                                        ->getAttribute(
                                            ProductAttributeInterface::ENTITY_TYPE_CODE,
                                            $stateName
                                        );
                                    $options = $attribute_->getSource()->getAllOptions();
                                    if (!empty($options)) {
                                        foreach ($options as $key => $value) {
                                            if (!empty($value['label'])) {
                                                if ($value['label'] == $icePimProduct['state']) {
                                                    $add_option_id = $value['value'];
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                    if ($add_option_id != false) {
                                        $this->productAction->updateAttributes([$icePimProduct['magentoId']], [$stateName => $add_option_id], 0);
                                    }
                                    if (!empty($icePimProduct['magentoId'])) {
                                        $this->productAction->updateAttributes([$icePimProduct['magentoId']], [$stateName => $add_option_id], 0);
                                        break;
                                    }
                                } else {
                                    $this->productAction->updateAttributes([$icePimProduct['magentoId']], [$stateName => $add_option_id], 0);
                                }
                            }
                            break;
                        case 'text':
                            $product_state_name = $product->getData($stateName);
                            if (empty($product_state_name)) {
                                $this->productAction->updateAttributes([$icePimProduct['magentoId']], [$stateName => $icePimProduct['state']], 0);
                            }
                            break;
                    }
                }

                if ($icePimProduct['productStatus'] == 'disabled') {
                    $product->setStatus(2);
                } else {
                    $product->setStatus(1);
                }

                if (array_key_exists('totalShop', $icePimProduct)) {
                    $totalShop = (int)$icePimProduct['totalShop'];
                    if ($totalShop === 1) {

                        $websiteLists = $this->storeManager->getWebsites();

                        if (!empty($websiteLists) && $websites == 0) {
                            foreach ($websiteLists as $value) {
                                $websiteIds[] = $value->getId();
                            }

                            $product->setWebsiteIds($websiteIds);
                            unset($websiteIds);

                        } else {
                            $product->setWebsiteIds([$websites]);
                        }

                        $product->setStoreId(0);

                        if ($icePimProduct['productStatus'] == 'disabled') {
                            $product->setStatus(2);
                        } else {
                            $product->setStatus(1);
                        }
                    }
                }

                if (array_key_exists('productShops', $icePimProduct)) {

                    $websiteIds = [];

                    if (!empty($icePimProduct['productShops'])) {
                        $productShops = explode(';', $icePimProduct['productShops']);

                        foreach ($productShops as $productShop) {
                            if (array_key_exists($productShop, $websiteList)) {
                                $websiteIds[] = $websiteList[$productShop];
                            }
                        }

                        $product->setWebsiteIds($websiteIds);

                        $product->setStoreId(0);

                        if ($icePimProduct['productStatus'] == 'disabled' || empty($websiteIds)) {
                            $product->setStatus(2);
                        } else {
                            $product->setStatus(1);
                        }
                    } else {
                        $product->setWebsiteIds($websiteIds);
                        $product->setStatus(2);
                    }
                }

                $product->setData('quantity_and_stock_status', ['qty' => $qty, 'is_in_stock' => $stockStatus]);

                $this->repository->save($product);

                $changedProduct[$productId] = $icePimProduct['magentoId'];

                $this->connectDB->saveConversions(
                    $productId,
                    $icePimProduct['magentoId'],
                    'products',
                    'imports_conversions_rules_logistics_updated'
                );
            } catch (\Throwable $exception) {
                $withError[$productId] = $icePimProduct['magentoId'] . ': ' . $exception->getMessage();
                continue;
            }
        }

        return json_encode(['products' => $changedProduct, 'withError' => $withError]);
    }

    public function createDeleteCategory($data)
    {
        $icePimProductsCats = json_decode($data, true);
        $multiLanguageUrlKey = $this->scopeConfigInterface->getValue(
            'iceshop_Icepimconnect_settings/icepimconnect_products_mapping/category_url_key'
        );

        $rootCategory = $this->scopeConfigInterface->getValue(
            'iceshop_Icepimconnect_settings/icepimconnect_products_mapping/products_root_category'
        );

        $categoryURLKeyBeautifier = $this->scopeConfigInterface->getValue(
            'iceshop_Icepimconnect_settings/icepimconnect_products_mapping/category_url_key_beautifier'
        );

        if (empty($categoryURLKeyBeautifier)) {
            $categoryURLKeyBeautifier = 'default';
        }

        if (empty($rootCategory) || !is_numeric($rootCategory)) {
            $rootCategory = 2;
        }

        $magentoCatIds = [];
        $magentoDeletedCatIds = [];
        $returnData = [];
        $err = [];

        $websites = $this->scopeConfigInterface->getValue(
            'iceshop_Icepimconnect_settings/icepimconnect_products_mapping/products_websites'
        );

        $websiteLists = $this->storeManager->getWebsites();

        if (!empty($websiteLists)) {
            foreach ($websiteLists as $value) {
                $websiteList[$value->getCode()] = $value->getId();
                $storeList[$value->getCode()] = $this->connectDB->objectManager->create('\Magento\Store\Api\StoreWebsiteRelationInterface')->getStoreByWebsiteId($value->getId());
                $storeListById[$value->getId()] = $this->connectDB->objectManager->create('\Magento\Store\Api\StoreWebsiteRelationInterface')->getStoreByWebsiteId($value->getId());
            }
        }

        $allStores = [];
        $allStores[] = 0; //default store

        if ($websites === "all") {
            $websites = 0;
            foreach ($this->storeManager->getStores() as $str) {
                $allStores[] = $str->getId();
            }
        } else {
            foreach ($storeListById[$websites] as $str) {
                $allStores[] = $str;
            }
        }

        foreach ($icePimProductsCats as $store_id => $icePimProductsCat) {
            if ($store_id === 'disabledIds') {
                if (!empty($icePimProductsCat)) {
                    $disabledIds = explode(';;', $icePimProductsCat);
                    foreach ($disabledIds as $disabledId) {
                        $categoryFactory = $this->categoryRepositoryInterface->get($disabledId, 0);
                        $this->storeManager->setCurrentStore(0);
                        $categoryFactory->setIsActive(0);
                        $this->categoryRepositoryInterface->save($categoryFactory);
                    }
                }
                continue;
            }

            if ($store_id === 'deletedIds') {
                if (!empty($icePimProductsCat)) {
                    $deletedIds = explode(';;', $icePimProductsCat);
                    foreach ($deletedIds as $deletedId) {
                        try {
                            $this->categoryRepositoryInterface->deleteByIdentifier($deletedId);
                            $magentoDeletedCatIds[] = $deletedId;
                        } catch (NoSuchEntityException $e) {
                            $magentoDeletedCatIds[] = $deletedId;
                            continue;
                        }
                    }
                }
                continue;
            }

            if (!in_array($store_id, $allStores)) {
                continue;
            }

            foreach ($icePimProductsCat as $pimCatId => $category) {
                $categoryTreeMagento = [];
                try {
                    $startingLevel = 2;
                    $parentCategoryId = $rootCategory;
                    $insurance = explode(';;', $icePimProductsCats[0][$pimCatId]['names']);
                    $categoryNames = explode(';;', $category['names']);
                    $categoryTreePim = explode(';;', $category['ids']);
                    $categorySortOrder = explode(';;', $category['sortorder']);
                    $pimAndMagentoCategoryIds = explode(';;', $category['magentoCatIds'] ?? '');
                    $activeCategories = explode(';;', $category['activeCategory']);

                    foreach ($categoryNames as $key => $categoryName) {
                        $categoryLevel = $key + $startingLevel;
                        $urlKeyName = $insurance[$key];
                        $parentCategory = $this->categoryRepositoryInterface->get($parentCategoryId);

                        $setUrlKey = ($categoryName != '') ? $this->connectDB->slugify($categoryName) : $this->connectDB->slugify($urlKeyName);
                        $setUrlKeyDefault = $this->connectDB->slugify($urlKeyName);

                        if (array_key_exists($key, $pimAndMagentoCategoryIds) && !empty($pimAndMagentoCategoryIds[$key])) {
                            $categoryFactory = $this->categoryRepositoryInterface->get($pimAndMagentoCategoryIds[$key], $store_id);
                            if (!empty($categoryFactory)) {
                                $this->storeManager->setCurrentStore($store_id);
                                $categoryFactory->setName($categoryName);
                                // $categoryFactory->setData('heading', 'test');
                                $categoryFactory->setStoreId($store_id);
                                $categoryFactory->setIsActive((int)$activeCategories[$key] === 0 ? 1:0);
                                if ((int)$store_id === 0) {
                                    $categoryFactory->setPosition($categorySortOrder[$key]);
                                    $categoryFactory->setParentId($parentCategory->getId());
                                    if ($categoryURLKeyBeautifier == 'iceshop') {
                                        $categoryFactory->setData('url_key', $setUrlKeyDefault);
                                    }
                                    $categoryFactory->setIsActive((int)$activeCategories[$key] === 0 ? 1:0);
                                    $categoryFactory->setPath($parentCategory->getPath() . '/');
                                } else {
                                    if ($categoryURLKeyBeautifier == 'iceshop') {
                                        $categoryFactory->setData('url_key', $setUrlKey);
                                    }
                                }
                                $categoryFactory->setLevel($categoryLevel);
                                $cat = $this->categoryRepositoryInterface->save($categoryFactory);
                                $this->magentoCatIds[$categoryTreePim[$key]] = (int)$cat->getId();
                                $parentCategoryId = $cat->getId();
                                $magentoCatIds[$cat->getId()] = $cat->getId();
                                $categoryTreeMagento[$categoryTreePim[$key]] = $cat->getId();
                                continue;
                            }
                        } elseif (array_key_exists($categoryTreePim[$key], $this->magentoCatIds) && !empty($this->magentoCatIds[$categoryTreePim[$key]])) {
                            $categoryFactory = $this->categoryRepositoryInterface->get($this->magentoCatIds[$categoryTreePim[$key]], $store_id);
                            if (!empty($categoryFactory)) {
                                $this->storeManager->setCurrentStore($store_id);
                                $categoryFactory->setName($categoryName);
                                // $categoryFactory->setData('heading', 'test');
                                $categoryFactory->setStoreId($store_id);
                                $categoryFactory->setIsActive((int)$activeCategories[$key] === 0 ? 1:0);
                                if ((int)$store_id === 0) {
                                    $categoryFactory->setPosition($categorySortOrder[$key]);
                                    $categoryFactory->setParentId($parentCategory->getId());
                                    if ($categoryURLKeyBeautifier == 'iceshop') {
                                        $categoryFactory->setData('url_key', $setUrlKeyDefault);
                                    }
                                    $categoryFactory->setIsActive((int)$activeCategories[$key] === 0 ? 1:0);
                                    $categoryFactory->setPath($parentCategory->getPath() . '/');
                                } else {
                                    if ($categoryURLKeyBeautifier == 'iceshop') {
                                        $categoryFactory->setData('url_key', $setUrlKey);
                                    }
                                }
                                $categoryFactory->setLevel($categoryLevel);
                                $cat = $this->categoryRepositoryInterface->save($categoryFactory);
                                $this->magentoCatIds[$categoryTreePim[$key]] = (int)$cat->getId();
                                $parentCategoryId = $cat->getId();
                                $magentoCatIds[$categoryTreePim[$key]] = $cat->getId();
                                $categoryTreeMagento[$categoryTreePim[$key]] = $cat->getId();
                                continue;
                            }
                        }
                        $categoryFactory = $this->categoryFactory->create();
                        $this->storeManager->setCurrentStore($store_id);
                        $categoryFactory->setName($categoryName);
                        // $categoryFactory->setData('heading', 'test');
                        $categoryFactory->setStoreId($store_id);
                        $categoryFactory->setPosition($categorySortOrder[$key]);
                        $categoryFactory->setParentId($parentCategory->getId());
                        $categoryFactory->setIsActive((int)$activeCategories[$key] === 0 ? 1:0);
                        $categoryFactory->setPath($parentCategory->getPath() . '/');
                        if ($multiLanguageUrlKey == 0) {
                            if ($categoryURLKeyBeautifier == 'iceshop') {
                                $categoryFactory->setData('url_key', $setUrlKeyDefault);
                            }
                        } else {
                            if ($categoryURLKeyBeautifier == 'iceshop') {
                                $categoryFactory->setData('url_key', $setUrlKey);
                            }
                        }
                        $categoryFactory->setLevel($categoryLevel);
                        $cat = $this->categoryRepositoryInterface->save($categoryFactory);
                        $magentoCatIds[$categoryTreePim[$key]] = $cat->getId();
                        $parentCategoryId = $cat->getId();
                        $categoryTreeMagento[$categoryTreePim[$key]] = $cat->getId();
                        $this->magentoCatIds[$categoryTreePim[$key]] = (int)$cat->getId();
                    }
                    $returnData[] = [
                        'categoryTreePim' => implode('/', $categoryTreePim),
                        'categoryTreeMagento' => implode('/', $categoryTreeMagento),
                        'magentoCatIds' => implode('/', $magentoCatIds),
                    ];
                    unset($categoryTreeMagento);

                } catch (\Throwable $e) {
                    $err[] = $category['ids'] . " = " . $e->getMessage();
                    continue;
                }
            }
        }

        $responseArray = [
            'magentoCatIds' => $this->magentoCatIds,
            'magentoDeletedCatIds' => $magentoDeletedCatIds,
            'errors' => $err
        ];
        return json_encode($responseArray);
    }

    /**
     * @param mixed $data
     * @return string|void
     */
    public function createUpdateContent($data)
    {
        ini_set('memory_limit', -1);
        ini_set('max_execution_time', 0);
        ini_set('post_max_size', '256M');

        $mpn = $this->scopeConfigInterface->getValue(
            'iceshop_Icepimconnect_settings/icepimconnect_products_mapping/products_mapping_mpn'
        );
        $brandName = $this->scopeConfigInterface->getValue(
            'iceshop_Icepimconnect_settings/icepimconnect_products_mapping/products_mapping_brand'
        );
        $ean = $this->scopeConfigInterface->getValue(
            'iceshop_Icepimconnect_settings/icepimconnect_products_mapping/products_mapping_gtin'
        );
        $urlKeyUpdate = $this->scopeConfigInterface->getValue(
            'iceshop_Icepimconnect_settings/icepimconnect_service_settings/products_url_key_update'
        );
        $urlKeyPrefix = $this->scopeConfigInterface->getValue(
            'iceshop_Icepimconnect_settings/icepimconnect_service_settings/products_url_key_prefix'
        );
        $updateBulletPoints = $this->scopeConfigInterface->getValue(
            'iceshop_Icepimconnect_settings/icepimconnect_service_settings/products_bullet_points_update'
        );
        $bulletPointsAttribute = $this->scopeConfigInterface->getValue(
            'iceshop_Icepimconnect_settings/icepimconnect_service_settings/products_bullet_points_attribute'
        );
        $updatePdfs = $this->scopeConfigInterface->getValue(
            'iceshop_Icepimconnect_settings/icepimconnect_service_settings/products_pdfs_update'
        );
        $pdfsAttribute = $this->scopeConfigInterface->getValue(
            'iceshop_Icepimconnect_settings/icepimconnect_service_settings/products_pdfs_attribute'
        );
        $updateVideos = $this->scopeConfigInterface->getValue(
            'iceshop_Icepimconnect_settings/icepimconnect_service_settings/products_videos_update'
        );
        $videosAttribute = $this->scopeConfigInterface->getValue(
            'iceshop_Icepimconnect_settings/icepimconnect_service_settings/products_videos_attribute'
        );
        $activeIce = $this->scopeConfigInterface->getValue(
            'iceshop_Icepimconnect_settings/icepimconnect_products_mapping/products_active_ice'
        );

        $response = [];
        $icePimProducts = json_decode($data, true);

        $websites = $this->scopeConfigInterface->getValue(
            'iceshop_Icepimconnect_settings/icepimconnect_products_mapping/products_websites'
        );

        $websiteLists = $this->storeManager->getWebsites();

        if (!empty($websiteLists)) {
            foreach ($websiteLists as $value) {
                $websiteList[$value->getCode()] = $value->getId();
                $storeList[$value->getCode()] = $this->connectDB->objectManager->create('\Magento\Store\Api\StoreWebsiteRelationInterface')->getStoreByWebsiteId($value->getId());
                $storeListById[$value->getId()] = $this->connectDB->objectManager->create('\Magento\Store\Api\StoreWebsiteRelationInterface')->getStoreByWebsiteId($value->getId());
            }
        }

        $attributes = [];

        $allStores = [];
        $allStores[] = 0; //default store

        if ($websites === "all") {
            $websites = 0;
            foreach ($this->storeManager->getStores() as $str) {
                $allStores[] = $str->getId();
            }
        } else {
            foreach ($storeListById[$websites] as $str) {
                $allStores[] = $str;
            }
        }

        foreach ($icePimProducts as $pimProductId => $pimProduct) {
            if ($pimProduct['magentoId'] === '') {
                $response[$pimProductId] = false;
                continue;
            }


            $brandForUrlKey = $pimProduct['brand_name'];
            $mpnForUrlKey = $pimProduct['mpn'];

            try {
                $product = $this->repository->getById((int)$pimProduct['magentoId']);
                if (!empty($activeIce)) {
                    $useIcecatData = $product->getAttributeText($activeIce) ?? '';
                    if ($useIcecatData != 'Yes') {
                        $response[$pimProductId] = 'Content update not allowed';
                        continue;
                    }
                }

                $product->setAttributeSetId($pimProduct['attributeSetId']);
                $this->repository->save($product);
            } catch (\Throwable $e) {
                $response[$pimProductId] = $e->getMessage();
                continue;
            }

            if (isset($pimProduct['selectedProductCategories'])) {
                $categories = explode(';', $pimProduct['selectedProductCategories']);
                $magentoProductId = $pimProduct['magentoId'];

                if (!empty($categories)) {
                    try {
                        $categoryTableName = $this->connectDB->resource->getTableName('catalog_category_product');
                        $this->connectDB->connection->query("DELETE FROM `{$categoryTableName}` WHERE `product_id` = {$magentoProductId}");

                        if (!empty($categories)) {
                            foreach ($categories as $category) {
                                $this->connectDB->connection->query("INSERT INTO `{$categoryTableName}` (`category_id`, `product_id`) VALUES ('{$category}', '{$magentoProductId}')");
                            }
                        }
                    } catch (\Throwable $e) {
                        continue;
                    }
                }
            }

            try {
                foreach ($allStores as $str) {
                    $bulletPoints = '';
                    if (!empty($pimProduct['bullet_points'][$str])) {
                        $bulletPoints = '<ul>';
                        foreach($pimProduct['bullet_points'][$str] as $bp) {
                            $bulletPoints.= '<li>' . $bp . '</li>';
                        }
                        $bulletPoints.= '</ul>';
                    }

                    $pdfs = '';
                    if (!empty($pimProduct['pdfs'][$str])) {
                        $pdfs = '<ul>';
                        foreach($pimProduct['pdfs'][$str] as $pdf) {
                            $pdfs.= '<li>' . $pdf . '</li>';
                        }
                        $pdfs.= '</ul>';
                    }

                    $videos = '';
                    if (!empty($pimProduct['videos'][$str])) {
                        $videos = '<ul>';
                        foreach($pimProduct['videos'][$str] as $video) {
                            $videos.= '<li>' . $video . '</li>';
                        }
                        $videos.= '</ul>';
                    }

                    $generateBulkAttributeData = [];
                    if (!empty($pimProduct['names'][$str])) {
                        $generateBulkAttributeData['name'] = $pimProduct['names'][$str];
                        $nameForUrlKey = $pimProduct['names'][$str];
                    }
                    if (isset($pimProduct['descriptions'][$str]['short_description'])) {
                        $generateBulkAttributeData['short_description'] = $pimProduct['descriptions'][$str]['short_description'] ?? '';
                    }
                    if (isset($pimProduct['descriptions'][$str]['long_description'])) {
                        $generateBulkAttributeData['description'] = $pimProduct['descriptions'][$str]['long_description'] ?? '';

                        if (array_key_exists('bullet_points', $pimProduct)) {
                            if ($updateBulletPoints == 1) {
                                $generateBulkAttributeData['description'].= $bulletPoints;
                            } elseif ($updateBulletPoints == 2 && !empty($bulletPointsAttribute)) {
                                $generateBulkAttributeData[$bulletPointsAttribute] = $bulletPoints;
                            }
                        }

                        if (array_key_exists('pdfs', $pimProduct)) {
                            if ($updatePdfs == 1) {
                                $generateBulkAttributeData['description'].= $pdfs;
                            } elseif ($updatePdfs == 2 && !empty($pdfsAttribute)) {
                                $generateBulkAttributeData[$pdfsAttribute] = $pdfs;
                            }
                        }

                        if (array_key_exists('videos', $pimProduct)) {
                            if ($updateVideos == 1) {
                                $generateBulkAttributeData['description'].= $videos;
                            } elseif ($updateVideos == 2 && !empty($videosAttribute)) {
                                $generateBulkAttributeData[$videosAttribute] = $videos;
                            }
                        }
                    }

                    if ($urlKeyPrefix == 0) {
                        $urlDef = $nameForUrlKey;
                    } elseif ($urlKeyPrefix == 1) {
                        $urlDef = $mpnForUrlKey . '-' . $nameForUrlKey;
                    } elseif ($urlKeyPrefix == 2) {
                        $urlDef = $brandForUrlKey . '-' . $nameForUrlKey;
                    } elseif ($urlKeyPrefix == 3) {
                        $urlDef = $mpnForUrlKey . '-' . $brandForUrlKey . '-' . $nameForUrlKey;
                    } elseif ($urlKeyPrefix == 4) {
                        $urlDef = $mpnForUrlKey;
                    } elseif ($urlKeyPrefix == 5) {
                        $urlDef = $brandForUrlKey . '-' . $mpnForUrlKey . '-' . $nameForUrlKey;
                    } elseif ($urlKeyPrefix == 6) {
                        $urlDef = $nameForUrlKey . '-' . $mpnForUrlKey;
                    } else {
                        $urlDef = $mpnForUrlKey . '-' . $brandForUrlKey . '-' . $nameForUrlKey;
                    }

                    if ($urlKeyUpdate == 1) {
                        $urlKey = $product->formatUrlKey($urlDef);
                        $generateBulkAttributeData['url_key'] = $urlKey;
                    }

                    $generateBulkAttributeData[$mpn] = $pimProduct['mpn'] ?? '';
                    $generateBulkAttributeData[$ean] = $pimProduct['ean'] ?? '';

                    $brand_name_attribute_type = $product->getResource()
                        ->getAttribute($brandName)
                        ->getFrontend()
                        ->getInputType();
                    switch ($brand_name_attribute_type) {
                        case 'select':
                        case 'dropdown':
                            $product_brand_name = $product->getAttributeText($brandName);
                            if (strtolower($product_brand_name) != strtolower($pimProduct['brand_name'])) {
                                $add_option_id = false;
                                $attribute_ = $this->connectDB->objectManager->create(
                                    'Magento\Eav\Model\Config'
                                )
                                    ->getAttribute(
                                        ProductAttributeInterface::ENTITY_TYPE_CODE,
                                        $brandName
                                    );
                                $options = $attribute_->getSource()->getAllOptions();
                                if (!empty($options)) {
                                    foreach ($options as $value) {
                                        if (!empty($value['label'])) {
                                            if ($value['label'] == $pimProduct['brand_name']) {
                                                $add_option_id = $value['value'];
                                                break;
                                            }
                                        }
                                    }
                                }
                                if ($add_option_id == false) {
                                    $attrId = $this->attribute->getIdByCode(
                                        ProductAttributeInterface::ENTITY_TYPE_CODE,
                                        $brandName
                                    );

                                    $option = [];
                                    $option['attribute_id'] = $attrId;
                                    $option['value'][$add_option_id ?? 0][0] = $pimProduct['brand_name'];

                                    $this->eavSetup->addAttributeOption($option);

                                    $add_option_id = false;

                                    $attribute_ = $this->connectDB->objectManager->create(
                                        'Magento\Eav\Model\Config'
                                    )
                                        ->getAttribute(
                                            ProductAttributeInterface::ENTITY_TYPE_CODE,
                                            $brandName
                                        );
                                    $options = $attribute_->getSource()->getAllOptions();
                                    if (!empty($options)) {
                                        foreach ($options as $key => $value) {
                                            if (!empty($value['label'])) {
                                                if ($value['label'] == $pimProduct['brand_name']) {
                                                    $add_option_id = $value['value'];
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                    if ($add_option_id != false) {
                                        $generateBulkAttributeData[$brandName] = $add_option_id;
                                    }
                                    if (!empty($pimProductId)) {
                                        $generateBulkAttributeData[$brandName] = $add_option_id;
                                        break;
                                    }
                                } else {
                                    $generateBulkAttributeData[$brandName] = $add_option_id;
                                }
                            }
                            break;
                        case 'text':
                            $product_brand_name = $product->getData($brandName);
                            if (empty($product_brand_name)) {
                                $generateBulkAttributeData[$brandName] = $pimProduct['brand_name'];
                            }
                            break;
                    }

                    try {
                        if (array_key_exists($str, $pimProduct['attributeValues'])) {
                            $attributes = $pimProduct['attributeValues'][$str];
                            if (!empty($attributes)) {
                                foreach ($attributes as $attrCode => $attribute) {
                                    if ($attribute['is_searchable']) {
                                        $add_option_id = false;
                                        $attribute_ = $this->connectDB->objectManager->create(
                                            'Magento\Eav\Model\Config'
                                        )
                                            ->getAttribute(
                                                ProductAttributeInterface::ENTITY_TYPE_CODE,
                                                $attrCode
                                            );
                                        $options = $attribute_->getSource()->getAllOptions();
                                        if (!empty($options)) {
                                            foreach ($options as $value) {
                                                if (!empty($value['label'])) {
                                                    if (array_key_exists($attrCode, $pimProduct['attributeValues'][0])) {
                                                        if ($value['label'] == $pimProduct['attributeValues'][0][$attrCode]['value']) {
                                                            $add_option_id = $value['value'];
                                                            break;
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                        if ($add_option_id == false) {
                                            $attrId = $this->attribute->getIdByCode(
                                                ProductAttributeInterface::ENTITY_TYPE_CODE,
                                                $attrCode
                                            );

                                            try {
                                                $option = [];
                                                $option['attribute_id'] = $attrId;
                                                if (array_key_exists($attrCode, $pimProduct['attributeValues'][0])) {
                                                    $option['value'][0][0] = $pimProduct['attributeValues'][0][$attrCode]['value'];
                                                    foreach ($allStores as $strID) {
                                                        if (!$strID) {
                                                            continue;
                                                        }
                                                        $option['value'][0][$strID] = !empty($pimProduct['attributeValues'][$strID][$attrCode]['value']) ? $pimProduct['attributeValues'][$strID][$attrCode]['value'] : '';
                                                    }

                                                    $this->eavSetup->addAttributeOption($option);
                                                }
                                            } catch (\Throwable $e) {
                                                $response[$attrCode] = $e->getMessage();
                                                continue;
                                            }

                                            $add_option_id = false;

                                            $attribute_ = $this->connectDB->objectManager->create(
                                                'Magento\Eav\Model\Config'
                                            )
                                                ->getAttribute(
                                                    ProductAttributeInterface::ENTITY_TYPE_CODE,
                                                    $attrCode
                                                );
                                            $options = $attribute_->getSource()->getAllOptions();
                                            if (!empty($options)) {
                                                foreach ($options as $key => $value) {
                                                    if (!empty($value['label'])) {
                                                        if ($value['label'] == $pimProduct['attributeValues'][$str][$attrCode]['value']) {
                                                            $add_option_id = $value['value'];
                                                            break;
                                                        }
                                                    }
                                                }
                                            }
                                            if ($add_option_id != false) {
                                                $attributes[$attrCode] = $add_option_id;

                                                $generateBulkAttributeData[$attrCode] = $add_option_id;
                                            }
                                            continue;
                                        }
                                        if ($add_option_id != false) {
                                            $attrId = $this->attribute->getIdByCode(
                                                ProductAttributeInterface::ENTITY_TYPE_CODE,
                                                $attrCode
                                            );

                                            try {
                                                $option = [];
                                                $option['attribute_id'] = $attrId;
                                                $option['value'][$add_option_id][0] = $pimProduct['attributeValues'][0][$attrCode]['value'];
                                                foreach ($allStores as $strID) {
                                                    if (!$strID) {
                                                        continue;
                                                    }
                                                    $option['value'][$add_option_id][$strID] = !empty($pimProduct['attributeValues'][$strID][$attrCode]['value']) ? $pimProduct['attributeValues'][$strID][$attrCode]['value'] : '';
                                                }

                                                $this->eavSetup->addAttributeOption($option);
                                            } catch (\Throwable $e) {
                                                $response[$attrCode] = $e->getMessage();
                                                continue;
                                            }

                                            $attributes[$attrCode] = $add_option_id;

                                            $generateBulkAttributeData[$attrCode] = $add_option_id;
                                        }
                                        continue;
                                    } else {
                                        $generateBulkAttributeData[$attrCode] = $attribute['value'];
                                    }
                                }

                                if (array_key_exists('multiShop', $pimProduct)) {
                                    if (array_key_exists('customFields', $pimProduct['multiShop'])) {
                                        $customFields = $pimProduct['multiShop']['customFields'][$str];
                                        foreach ($customFields as $field => $customField) {
                                            $splitField = explode('_', $field);
                                            $shopCode = current($splitField);
                                            if (array_key_exists($shopCode, $storeList)) {
                                                $fieldCode = str_replace($shopCode . '_', '', $field);
                                                $fieldValue = html_entity_decode(strip_tags($customField));
                                                $generateBulkAttributeData[$fieldCode] = $fieldValue;
                                            }
                                        }
                                    }
                                }

                                if (array_key_exists('singleShop', $pimProduct)) {
                                    if (array_key_exists('customFields', $pimProduct['singleShop'])) {
                                        $customFields = $pimProduct['singleShop']['customFields'][$str];
                                        foreach ($customFields as $field => $customField) {
                                            $fieldCode = $field;
                                            $fieldValue = html_entity_decode(strip_tags($customField));
                                            $generateBulkAttributeData[$fieldCode] = $fieldValue;
                                        }
                                    }
                                }
                            }

                            if ($pimProduct['totalShop'] > 1) {
                                $loadProductForPrices = $this->productRepository->getById((int)$pimProduct['magentoId'], false, $str, false);
                                $price = $loadProductForPrices->getData('price');
                                $specialPrice = $loadProductForPrices->getData('special_price');
                                $generateBulkAttributeData['price'] = $price;
                                $generateBulkAttributeData['special_price'] = $specialPrice;
                            }

                            $this->productAction->updateAttributes([(int)$pimProduct['magentoId']], $generateBulkAttributeData, $str);
                        }
                    } catch (\Throwable $e) {
                        $response[$pimProductId] = $e->getMessage();
                        break;
                    }

                    $response[$pimProductId] = true;
                }
            } catch (\Throwable $e) {
                $response[$pimProductId] = $e->getMessage();
                break;
            }

            $this->connectDB->saveConversions(
                $pimProductId,
                (int)$pimProduct['magentoId'],
                'products',
                'imports_conversions_rules_content_updated'
            );
        }

        return json_encode($response);
    }

    /**
     * @return string|void
     */
    public function testing()
    {
        // testing
    }

    public function linkProductRelations($data)
    {
        $useInsteadOfSku = $this->scopeConfigInterface->getValue(
            'iceshop_Icepimconnect_settings/icepimconnect_products_mapping/products_use_instead_of_sku'
        );
        if ($useInsteadOfSku == 'sku') {
            $products = json_decode($data, true);

            foreach ($products as $productSku => $relations) {
                try {
                    $producRelations = [];
                    if (isset($relations['related'])) {
                        $product = $this->repository->get($productSku, true, 0, true);
                        foreach ($relations['related'] as $relation) {
                            $linkData = $this->productLinkInterface->setSku($productSku)
                                ->setLinkedProductSku($relation)
                                ->setLinkType('crosssell');
                            $producRelations[] = $linkData;
                        }
                        $product->setProductLinks($producRelations);
                        $this->repository->save($product);
                    }


                    unset($producRelations);
                    $producRelations = [];

                    $product = $this->repository->get($productSku, true, 0, true);

                    if (!empty($relations['related'])) {
                        $producRelations = $product->getProductLinks();
                    }
                    if (isset($relations['preferred'])) {
                        foreach ($relations['preferred'] as $relation) {
                            $linkData = $this->productLinkInterface->setSku($productSku)
                                ->setLinkedProductSku($relation)
                                ->setLinkType('related');
                            $producRelations[] = $linkData;
                        }
                        $product->setProductLinks($producRelations);
                        $this->repository->save($product);
                    }

                    unset($producRelations);

                    $producRelations = [];

                    $product = $this->repository->get($productSku, true, 0, true);

                    if (!empty($relations['related']) || !empty($relations['preferred'])) {
                        $producRelations = $product->getProductLinks();
                    }

                    if (isset($relations['alternatives'])) {
                        foreach ($relations['alternatives'] as $relation) {
                            $linkData = $this->productLinkInterface->setSku($productSku)
                                ->setLinkedProductSku($relation)
                                ->setLinkType('upsell');
                            $producRelations[] = $linkData;
                        }
                        $product->setProductLinks($producRelations);
                        $this->repository->save($product);
                    }

                    unset($producRelations);
                } catch (\Throwable $exception) {
                    continue;
                }
            }
        }
    }
}
