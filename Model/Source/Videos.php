<?php
namespace Iceshop\Icepimconnect\Model\Source;

class Videos implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            '0' => 'Off',
            '1' => 'Add video urls below long description',
            '2' => 'Add video urls to custom attribute'
        ];
    }
}
