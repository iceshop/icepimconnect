<?php
namespace Iceshop\Icepimconnect\Model\Source;

use Iceshop\Icepimconnect\Model\ICEpimConnect;

class IndexerMode implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ICEpimConnect::INDEXER_MODE_SCHEDULED => __('Scheduled'),
            ICEpimConnect::INDEXER_MODE_UPDATE_ON_SAVE => __('Update on save'),
        ];
    }
}


