<?php
namespace Iceshop\Icepimconnect\Model\Source;

class Beautifier implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            'default' => 'Default Magento2 URL Formatter',
            'iceshop' => 'Custom URL Formatter',
        ];
    }
}
