<?php
namespace Iceshop\Icepimconnect\Model\Source;

class BulletPoints implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            '0' => 'Off',
            '1' => 'Add bullet points below long description',
            '2' => 'Add bullet points to custom attribute'
        ];
    }
}
