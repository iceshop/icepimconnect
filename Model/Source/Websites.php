<?php

namespace Iceshop\Icepimconnect\Model\Source;

use \Magento\Store\Model\StoreManager;

class Websites implements \Magento\Framework\Option\ArrayInterface
{

    protected $_urlBuilder;

    public function __construct(
        \Magento\Backend\Model\UrlInterface $urlBuilder
    )
    {
        $this->_urlBuilder = $urlBuilder;
    }

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {

        $return = [
            '' => "--- " . __('Choose website') . " ---"
        ];

        $om = \Magento\Framework\App\ObjectManager::getInstance();
        /** @var \Magento\Store\Model\StoreManagerInterface|\Magento\Store\Model\StoreManager $storeManager */
        $storeManager = $om->get('Magento\Store\Model\StoreManagerInterface');
        $websites = $storeManager->getWebsites();

        $return['all'] = 'all websites';

        if (!empty($websites)) {
            foreach ($websites as $key => $value) {
                $return[$key] = $value->getName();
            }
        }

        return $return;
    }

}
