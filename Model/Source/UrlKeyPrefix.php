<?php
namespace Iceshop\Icepimconnect\Model\Source;

class UrlKeyPrefix implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            '0' => 'Product Name',
            '1' => 'MPN + Product Name',
            '2' => 'Brand + Product Name',
            '3' => 'MPN + Brand + Product Name',
            '5' => 'Brand + MPN + Product Name',
            '4' => 'MPN',
            '6' => 'Product Name + MPN'
        ];
    }
}
