<?php
namespace Iceshop\Icepimconnect\Model\Source;

class PDFs implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            '0' => 'Off',
            '1' => 'Add PDF urls below long description',
            '2' => 'Add PDF urls to custom attribute'
        ];
    }
}
