<?php

namespace Iceshop\Icepimconnect\Model\Source;

use \Magento\Store\Model\StoreManager;

class RootCategories implements \Magento\Framework\Option\ArrayInterface
{

    protected $_urlBuilder;

    public function __construct(
        \Magento\Backend\Model\UrlInterface $urlBuilder
    )
    {
        $this->_urlBuilder = $urlBuilder;
    }

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {

        $return = [
            '' => "--- " . __('Choose Root Category') . " ---"
        ];

        $om = \Magento\Framework\App\ObjectManager::getInstance();
        $storeManager = $om->get('Magento\Store\Model\StoreManagerInterface');
        foreach ($storeManager->getStores() as $store) {
            $catId = $store->getRootCategoryId();
            $category = $om->get('\Magento\Catalog\Model\CategoryFactory')->create()->load($catId);
            $return[$catId] = $category->getName();
        }

        return $return;
    }

}
