<?php
namespace Iceshop\Icepimconnect\Model\Source;

class SkuFields implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            'sku' => 'SKU',
            'mpn' => 'MPN',
        ];
    }
}
