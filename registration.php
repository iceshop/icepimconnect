<?php

/**
 * @author ICEShop Team
 * @copyright Copyright (c) 2016 ICEShop (https://www.iceshop.biz/)
 * @package ICEShop_ICECatConnect
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Iceshop_Icepimconnect',
    __DIR__
);
