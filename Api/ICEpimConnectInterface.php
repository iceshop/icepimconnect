<?php

namespace Iceshop\Icepimconnect\Api;

interface ICEpimConnectInterface
{
    /**
     * @return mixed
     */
    public function flushCache();

    /**
     * Check version of module
     *
     * @return string
     */
    public function getICEshopIcepimconnectorExtensionVersion();

    /**
     * Get products from shop
     * @param  mixed $data
     * @return string
     */
    public function getProductsBatch($data);

    /**
     * Getting languages settings
     * @return string
     */
    public function getLanguageMapping();

    /**
     * Retrieve attribute set list
     * @param  mixed $data
     * @return string
     */
    public function catalogProductAttributeSetList($data);

    /**
     * Fetching attribute lists for attribute set batch
     *
     * @param mixed
     * @return string
     */
    public function getProductAttributeList($data);

    /**
     * Save products data
     * @param  mixed $data
     * @return string
     */
    public function saveAttributeSetBatch($data);

    /**
     * Method to save updated products data as a batch (speeds up import process)
     *
     * @param mixed $data
     * @return string
     */
    public function saveProductsBatch($data);

    /**
     * Add products images queue
     *
     * @param mixed $data
     * @return string
     */
    public function queueProductsImages($data);

    /**
     * Update language codes
     *
     * @param mixed $data
     * @return string
     */

    public function updateLanguageCodes($data);

    /**
     * Set indexer mode
     *
     * @param mixed $data
     * @return mixed
     */
    public function setIndexerMode($data);

    /**
     * Run full reindex
     *
     * @return string
     */
    public function runFullReindex();

    /**
     * Download images
     * @param mixed $data
     * @return string
     */
    public function processProductsImagesQueue($data);

    /**
     * @return mixed
     */
    public function fixDefaultImages();

    /**
     * @return mixed
     */
    public function cleanUnusedImages();

    /**
     * Try re-upload images
     * @param mixed $data
     * @return string
     */
    public function processBrokenProductsImages($data);

    /**
     * Fix failed attributes
     *
     * @param mixed $data
     * @return string
     */
    public function fixFailedAttributes();

    /**
     * Set image as default
     *
     * @param mixed $data
     * @return string
     */
    public function processDefaultProductsImages($data);

    /**
     * Method to save updated products data as a batch (speeds up import process)
     *
     * @param mixed $data
     * @return string
     */
    public function saveMainAttributesBatch($data);

    /**
     * Attributes refresh
     *
     * @param mixed $data
     * @return string
     */
    public function attributesRefresh($data);

    /**
     * @return mixed
     */
    public function setCategoryIsActive();

    /**
     * Get all exported products
     * @return mixed
     */
    public function getProductConversions();

    /**
     * @param mixed $data
     * @return string|void
     */
    public function saveConversionIds($data);

    /**
     * Create read update delete products
     *
     * @param mixed $data
     * @return string
     */
    public function crudProducts($data);

    /**
     * Update price and stock of product
     *
     * @param mixed $data
     * @return string
     */
    public function priceAndStockUpdate($data);

    /**
     * @return mixed
     */
    public function reIndex();

    /**
     * @param mixed $data
     * @return string
     */
    public function createUpdateContent($data);

    /**
     * @param mixed $data
     * @return string
     */
    public function createDeleteCategory($data);


    /**
     * @return string
     */
    public function testing();

    /**
     * @param mixed $data
     * @return string
     */
    public function linkProductRelations($data);

}
