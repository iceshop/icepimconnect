<?php

namespace Iceshop\Icepimconnect\Controller\Adminhtml\Data;

use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\App\ProductMetadata;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Config\ConfigOptionsListConstants;
use Magento\Framework\Indexer\StateInterface;
use \Magento\Framework\App\ObjectManager;
use Iceshop\Icepimconnect\Model\ICEpimConnectDB;

class Index extends \Magento\Framework\App\Action\Action
{

    private $connection;

    private $resource;

    /**
     * @var \Magento\Framework\Module\ResourceInterface
     */
    protected $moduleResource;

    protected $resultPageFactory;

    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory resultPageFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Module\ResourceInterface $moduleResource
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->moduleResource = $moduleResource;
    }

    public function getConnection()
    {
        if (!$this->connection) {
            $resource = ObjectManager::getInstance()->create('\Magento\Framework\App\ResourceConnection');
            $this->connection = $resource->getConnection(
                \Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION
            );
        }
        return $this->connection;
    }

    private function _getResource()
    {
        if (!$this->resource) {
            $this->resource = ObjectManager::getInstance()->create('\Magento\Framework\App\ResourceConnection');
        }
        return $this->resource;
    }

    public function execute()
    {

        $this->_getResource();
        $this->getConnection();

        $block = [];

        $resultPage = $this->resultPageFactory->create();

        $block['icepimconnect_information'] = $resultPage->getLayout()
            ->createBlock('Iceshop\Icepimconnect\Block\GridBlock')
            ->generateTable($this->_getProductsStatistics());

        $block['attributes_content_statistic'] = $resultPage->getLayout()
            ->createBlock('Iceshop\Icepimconnect\Block\GridBlock')
            ->generateTable($this->_getAttributeStatistics());

        $block['image_statistic'] = $resultPage->getLayout()
            ->createBlock('Iceshop\Icepimconnect\Block\GridBlock')
            ->generateTable($this->_getImagesStatistics());

        $block['current_version_module'] = $resultPage->getLayout()
            ->createBlock('Iceshop\Icepimconnect\Block\GridBlock')
            ->generateTable($this->_getVersion());

//        $block['icepimconnect_information'] = 'Preparing';
//        $block['attributes_content_statistic'] = 'Preparing';
//        $block['current_version_module'] = 'Preparing';

        $jsonData = json_encode($block);
        $this->getResponse()->setHeader('Content-type', 'application/json');
        $this->getResponse()->setBody($jsonData);
    }

    private function _getProductsStatistics()
    {
        $icecatConnectDB = new ICEpimConnectDB();

        $return = [];
        $allProductsQuery = $this->connection->query("SELECT COUNT(1) as cnt FROM `icecat_imports_conversions_rules_products`");
        $allProductsResult = [];
        while ($row = $allProductsQuery->fetch()) {
            $allProductsResult[] = $row;
        }

        $allProducts = (isset($allProductsResult[0]['cnt'])) ? $allProductsResult[0]['cnt'] : 0;

//        $mappedProductsQuery = $this->connection->query("SELECT count(*) as mappedProducts FROM {$this->resource
//        ->getTableName('catalog_product_entity')} WHERE updated_ice <> '0000-00-00 00:00:00'");
//        $mappedProductsResult = [];
//        while ($row = $mappedProductsQuery->fetch()) {
//            $mappedProductsResult[] = $row;
//        }
//        $mappedProducts = (isset($mappedProductsResult[0]['mappedProducts'])) ?
//            $mappedProductsResult[0]['mappedProducts'] : 0;
//        $percentMapped = 0;
//        if ((!empty($allProducts)) && (!empty($mappedProducts))) {
//            $percentMapped = round((($mappedProducts / $allProducts) * 100), 2);
//        }

        $lastStarted = $icecatConnectDB->getDbVariable('icepimconnect_content_last_start');
        $lastFinished = $icecatConnectDB->getDbVariable('icepimconnect_content_last_finish');

        $return['Total products amount'] = $allProducts;
//        $return['Mapped Products Amount'] = $mappedProducts . ' (' . $percentMapped . '%)';

        $return['Import last started'] = (!empty($lastStarted)) ? $lastStarted : __('Not started');
        $return['Import last finished'] = (!empty($lastFinished)) ? $lastFinished : __('Not finished');

//        $notMatchedProductsUrl = ObjectManager::getInstance()->create('\Magento\Framework\UrlInterface')->getUrl('iceshop_icepimconnect/data/downloadnotmatchedproducts');
//        $return['Download not matched products'] = '
//            <button id="download_sample_file" onclick="window.location.href=\'' . $notMatchedProductsUrl . '\';"  title="Download not matched products list" type="button" class="primary">
//                <span class="">
//                    <span>Download unmatched products list</span>
//                </span>
//            </button>';
        return $return;
    }

    /**
     * Get attribute statistic
     * @return array
     */
    private function _getAttributeStatistics()
    {
        $return = [];

        $attributeSetResult = [];
        $attributeSetQuery = $this->connection->query("SELECT COUNT(1) as cnt FROM `icecat_imports_conversions_rules_attribute_set`");
        while ($row = $attributeSetQuery->fetch()) {
            $attributeSetResult[] = $row;
        }

        $attributeSet = (isset($attributeSetResult[0]['cnt'])) ? $attributeSetResult[0]['cnt'] : 0;

        $attributesResult = [];
        $attributesQuery = $this->connection->query("SELECT COUNT(1) as cnt FROM `icecat_imports_conversions_rules_attribute`");
        while ($row = $attributesQuery->fetch()) {
            $attributesResult[] = $row;
        }

        $attributes = (isset($attributesResult[0]['cnt'])) ? $attributesResult[0]['cnt'] : 0;

        $attributeGroupResult = [];
        $attributeGroupQuery = $this->connection->query("SELECT COUNT(1) as cnt FROM `icecat_imports_conversions_rules_attribute_group`");
        while ($row = $attributeGroupQuery->fetch()) {
            $attributeGroupResult[] = $row;
        }

        $attributeGroup = (isset($attributeGroupResult[0]['cnt'])) ? $attributeGroupResult[0]['cnt'] : 0;

        $return['Attribute Sets Created'] = $attributeSet;
        $return['Attributes Created'] = $attributes;
        $return['Attribute Groups Created'] = $attributeGroup;

        return $return;
    }

    /**
     * Get image statistic on Icecatconnect Information
     * @return array
     */
    private function _getImagesStatistics()
    {
        $return = [];

        $allImagesResult = [];
        $allImagesQuery = $this->connection->query("SELECT COUNT(1) as cnt FROM `icecat_products_images`");
        while ($row = $allImagesQuery->fetch()) {
            $allImagesResult[] = $row;
        }

        $allImages = (isset($allImagesResult[0]['cnt'])) ? $allImagesResult[0]['cnt'] : 0;

        $downloadedResult = [];
        $downloadedQuery = $this->connection->query("SELECT COUNT(1) as cnt FROM `icecat_products_images` WHERE `internal_url` <> ''");
        while ($row = $downloadedQuery->fetch()) {
            $downloadedResult[] = $row;
        }

        $downloaded = (isset($downloadedResult[0]['cnt'])) ? $downloadedResult[0]['cnt'] : 0;

        $brokenResult = [];
        $brokenQuery = $this->connection->query("SELECT COUNT(1) as cnt FROM `icecat_products_images` WHERE `internal_url` = '' AND `broken` = 1");
        while ($row = $brokenQuery->fetch()) {
            $brokenResult[] = $row;
        }

        $broken = (isset($brokenResult[0]['cnt'])) ? $brokenResult[0]['cnt'] : 0;

        $deletedQuery = $this->connection->fetchAll("SELECT COUNT(1) as cnt FROM `icecat_products_images` WHERE `deleted` = 1");
        $deleted = (isset($deletedQuery[0]['cnt'])) ? $deletedQuery[0]['cnt'] : 0;

        $return['Total Images Entries'] = $allImages - $deleted;
        $return['Total Images Downloaded'] = $downloaded;
        $return['Images Waiting Download'] = abs($allImages - $downloaded);

        $brokenPercent = 0;

        if ($broken != 0) {
            $brokenPercent = round((($broken / $allImages) * 100), 2);
            $return['Images Waiting Download'] .= ' incl. ' . $broken . ' (' . $brokenPercent . '%) broken images';
        }

        return $return;
    }

    private function _getVersion()
    {
        $version = $this->moduleResource->getDbVersion('Iceshop_Icepimconnect');

        $return['Icepimconnect Version'] = $version;

        return $return;
    }
}
